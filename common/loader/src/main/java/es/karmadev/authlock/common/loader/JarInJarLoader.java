package es.karmadev.authlock.common.loader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * ClassLoader which loads an internal jar.
 * The custom class loader allows to append dependencies
 * during runtime without "hacking" nor modifying the
 * main class loader, which can cause security issues or even
 * malfunction of the application
 */
public class JarInJarLoader extends URLClassLoader {

    static {
        ClassLoader.registerAsParallelCapable();
    }

    /**
     * Create the jar-in-jar loader
     *
     * @param loader the parent loader
     * @param resource the jar-in-jar path
     */
    public JarInJarLoader(final ClassLoader loader, final String resource) {
        super(new URL[]{extract(loader, resource)}, loader);
    }

    /**
     * Add the specified URL to
     * the classpath
     *
     * @param url the URL to add
     */
    public void addJarToClasspath(final URL url) {
        addURL(url);
    }

    /**
     * Remove the jar resource
     */
    public void deleteJarResource() {
        URL[] urls = getURLs();
        if (urls.length == 0) {
            return;
        }

        try {
            Path path = Paths.get(urls[0].toURI());
            Files.deleteIfExists(path);
        } catch (Exception ignored) {
            //We just ignore the exception if we fail
            //to set deleting on exit
        }
    }

    /**
     * Instantiate the bootstrap
     *
     * @param bootstrapClass the bootstrap class
     * @param loaderTyper the loader type
     * @param loader the loader
     * @return the bootstrap
     * @param <T> the bootstrapper type
     */
    public <T> AuthLockBootstrap instantiate(final String bootstrapClass, final Class<T> loaderTyper, final T loader) {
        Class<?> clazz;
        try {
            clazz = Class.forName(bootstrapClass);
        } catch (ReflectiveOperationException ex) {
            throw new LoadingException("Failed to load bootstrap class", ex);
        }

        if (!AuthLockBootstrap.class.isAssignableFrom(clazz))
            throw new LoadingException("Bootstrap class " + bootstrapClass + " is not an AuthLock bootstrapper");

        Class<? extends AuthLockBootstrap> bootstrapper = clazz.asSubclass(AuthLockBootstrap.class);
        Constructor<? extends AuthLockBootstrap> bootstrapConstructor;
        try {
            bootstrapConstructor = bootstrapper.getConstructor(loaderTyper);
        } catch (ReflectiveOperationException ex) {
            throw new LoadingException("Failed to obtain bootstrap constructor", ex);
        }

        try {
            return bootstrapConstructor.newInstance(loader);
        } catch (ReflectiveOperationException ex) {
            throw new LoadingException("Failed to create bootstrapped plugin", ex);
        }
    }

    /**
     * Extracts the "jar-in-jar" from the loader plugin into a temporary file,
     * then returns a URL that can be used by the {@link JarInJarLoader}.
     *
     * @param loaderClassLoader the classloader for the "host" loader plugin
     * @param jarResourcePath the inner jar resource path
     * @return a URL to the extracted file
     */
    private static URL extract(ClassLoader loaderClassLoader, String jarResourcePath) throws LoadingException {
        URL jarInJar = loaderClassLoader.getResource(jarResourcePath);
        if (jarInJar == null) {
            throw new LoadingException("Could not locate jar-in-jar");
        }

        Path path;
        try {
            path = Files.createTempFile("authlock-jarinjar", ".jar.tmp");
        } catch (IOException e) {
            throw new LoadingException("Unable to create a temporary file", e);
        }

        path.toFile().deleteOnExit();

        try (InputStream in = jarInJar.openStream()) {
            Files.copy(in, path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new LoadingException("Unable to copy jar-in-jar to temporary path", e);
        }

        try {
            return path.toUri().toURL();
        } catch (MalformedURLException e) {
            throw new LoadingException("Unable to get URL from path", e);
        }
    }
}
