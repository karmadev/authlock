package es.karmadev.authlock.common.loader;

/**
 * Plugin bootstrapper
 */
public interface AuthLockBootstrap {

    /**
     * When the bootstrap is
     * loaded
     */
    void onLoad();

    /**
     * When the bootstrap is
     * enabled
     */
    default void onEnable() {}

    /**
     * When the bootstrap is
     * disabled
     */
    default void onDisable() {}
}
