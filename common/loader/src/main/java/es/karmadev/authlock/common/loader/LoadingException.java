package es.karmadev.authlock.common.loader;

/**
 * Runtime exception used if there is a problem during loading
 */
public class LoadingException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param message the exception message
     */
    public LoadingException(final String message) {
        super(message);
    }

    /**
     * Create the exception
     *
     * @param message the exception message
     * @param cause the exception cause
     */
    public LoadingException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
