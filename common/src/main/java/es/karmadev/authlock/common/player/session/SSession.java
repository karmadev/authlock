/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.session;

import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;

public class SSession implements Session {

    private static final AuthLock plugin = AuthLock.getInstance();
    private static final String SET_FIELD_ERROR = "Failed to update session field";

    private final UnsignedBigInteger id;
    private final UnsignedBigInteger clientId;
    private final SessionDAO dao;

    private boolean password;
    private boolean pin;
    private boolean totp;
    private Instant validity;

    public SSession(final UnsignedBigInteger id, final UnsignedBigInteger clientId, final DatabaseConnector connector,
                    final boolean password, final boolean pin, final boolean totp, final Instant validity) throws InitializationRequiredException {
        Preconditions.assertNotNull(id, "id");
        Preconditions.assertNotNull(clientId, "clientId");
        Preconditions.assertNotNull(connector, "connector");

        this.id = id;
        this.clientId = clientId;
        this.dao = new SessionDAO(connector);

        this.password = password;
        this.pin = pin;
        this.totp = totp;
        this.validity = validity;
    }

    /**
     * Get the SQL entity id
     *
     * @return the entity id
     */
    @Override
    public @NotNull UnsignedBigInteger getEntityId() {
        return this.id;
    }

    /**
     * Get the client of the session
     *
     * @return the session client
     */
    @Override
    public @NotNull UnsignedBigInteger getClientId() {
        return this.clientId;
    }

    /**
     * Get if the session is
     * password logged
     *
     * @return if password logged
     */
    @Override
    public boolean isPasswordLogged() {
        return this.password;
    }

    /**
     * Get if the session is
     * pin logged
     *
     * @return if pin logged
     */
    @Override
    public boolean isPinLogged() {
        return this.pin;
    }

    /**
     * Get if the session is
     * totp logged
     *
     * @return if totp logged
     */
    @Override
    public boolean isTotpLogged() {
        return this.totp;
    }

    /**
     * Get the session validity.
     * The session validity just represents
     * the last time {@link #setPasswordLogged(boolean)} was
     * successful for a status value of <code>true</code>
     *
     * @return the session validity
     */
    @Override
    public @NotNull Instant getValidity() {
        return Preconditions.getOrElse(this.validity, Instant.MIN);
    }

    /**
     * Set the account password
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @Override
    public @NotNull CompletableFuture<Boolean> setPasswordLogged(final boolean status) {
        if (this.password == status)
            return CompletableFuture.completedFuture(false);

        return this.dao.updatePassword(this.id, status).thenApply(rs -> {
            this.password = status;
            if (rs != null)
                this.validity = rs;

            return true;
        }).exceptionally(error -> {
            assert plugin != null;

            plugin.getLogger().error(error, SET_FIELD_ERROR);
            return false;
        });
    }

    /**
     * Set the account pin
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @Override
    public @NotNull CompletableFuture<Boolean> setPinLogged(final boolean status) {
        if (this.pin == status)
            return CompletableFuture.completedFuture(false);

        return this.dao.updatePin(this.id, status).thenApply(rs -> {
            this.pin = status;
            return true;
        }).exceptionally(error -> {
            assert plugin != null;

            plugin.getLogger().error(error, SET_FIELD_ERROR);
            return false;
        });
    }

    /**
     * Set the account totp
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @Override
    public @NotNull CompletableFuture<Boolean> setTotpLogged(final boolean status) {
        if (this.totp == status)
            return CompletableFuture.completedFuture(false);

        return this.dao.updateTotp(this.id, status).thenApply(rs -> {
            this.totp = status;
            return true;
        }).exceptionally(error -> {
            assert plugin != null;

            plugin.getLogger().error(error, SET_FIELD_ERROR);
            return false;
        });
    }
}
