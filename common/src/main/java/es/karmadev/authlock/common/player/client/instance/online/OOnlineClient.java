/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client.instance.online;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.player.client.instance.offline.OOfflineClient;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServer;

import java.time.Instant;

public class OOnlineClient extends OOfflineClient implements OnlineClient {

    private final OnlineClientBridge clientBridge;

    public OOnlineClient(final UnsignedBigInteger id, final Account account, final Session session, final Instant creation,
                         final OnlineClientBridge bridge, final DatabaseConnector connector) {
        super(id, account, session, creation, connector);
        this.clientBridge = bridge;
    }

    /**
     * Send a message to the client
     *
     * @param message the message to
     *                send
     */
    @Override
    public void sendMessage(final String message) {
        this.clientBridge.sendMessage(message);
    }

    /**
     * Disconnect the client
     *
     * @param reason the disconnect reason
     */
    @Override
    public void disconnect(final String reason) {
        this.clientBridge.kick(reason);
    }

    /**
     * Get if the client is
     * currently connected
     *
     * @return if the client is
     * connected
     */
    @Override
    public boolean isConnected() {
        return this.clientBridge.isConnected();
    }

    public static OnlineClientBuilder createOnlineBuilder(final DatabaseConnector connector) {
        return new OnlineClientBuilder(connector);
    }

    public static class OnlineClientBuilder extends OfflineClientBuilder {

        private OnlineClientBridge bridge;

        private OnlineClientBuilder(final DatabaseConnector connector) {
            super(connector);
        }

        @Override
        public OnlineClientBuilder id(final UnsignedBigInteger id) {
            this.id = id;
            return this;
        }

        @Override
        public OnlineClientBuilder name(final String name) {
            this.name = name;
            return this;
        }

        @Override
        public OnlineClientBuilder uuid(final UUIDStore store) {
            this.uuidStore = store;
            return this;
        }

        @Override
        public OnlineClientBuilder account(final Account account) {
            this.account = account;
            return this;
        }

        @Override
        public OnlineClientBuilder session(final Session session) {
            this.session = session;
            return this;
        }

        @Override
        public OnlineClientBuilder server(final SServer server) {
            this.server = server;
            return this;
        }

        @Override
        public OnlineClientBuilder creation(final Instant creation) {
            this.creation = creation;
            return this;
        }

        public OnlineClientBuilder bridge(final OnlineClientBridge bridge) {
            this.bridge = bridge;
            return this;
        }

        @Override
        public OOnlineClient build() {
            Preconditions.assertNotNull(name, "name");
            Preconditions.assertNotNull(uuidStore, "uuidStore");
            Preconditions.assertNotNull(server, "server");
            Preconditions.assertNotNull(bridge, "bridge");

            OOnlineClient client = new OOnlineClient(this.id, this.account, this.session, this.creation,
                    this.bridge, this.connector);
            client.name = name;
            client.uuidStore = uuidStore;
            client.server = server;

            return client;
        }
    }
}
