/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.player.account.AAccount;
import es.karmadev.authlock.common.player.client.instance.offline.OOfflineClient;
import es.karmadev.authlock.common.player.session.SSession;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jooq.Record;
import org.jooq.*;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.types.ULong;

import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.*;

@SuppressWarnings("unused")
final class ClientManagerDAO {

    private static final AuthLock plugin = AuthLock.getInstance();
    private final DatabaseConnector connector;

    public ClientManagerDAO(final DatabaseConnector connector) {
        if (plugin == null)
            throw new InitializationRequiredException(ClientManagerDAO.class, "ClientManagerDAO(DatabaseConnector)");

        this.connector = connector;
    }

    public CompletableFuture<Collection<OfflineClient>> getAllClients() {
        return CompletableFuture.supplyAsync(() -> {
            Set<OfflineClient> clients = new HashSet<>();

            DSLContext context = connector.getContext();
            try {
                SelectOnConditionStep<Record19<ULong, String, ULong, Timestamp,
                        ULong, String, String, String, Timestamp, String, Timestamp,
                        ULong, Boolean, Boolean, Boolean, Timestamp,
                        String, String, String>> step = context.select(
                                bigInt(user().getTableName(), user().getColumnName(IDENTIFIER)).as("user_id"),
                                string(user().getTableName(), user().getColumnName(USERNAME)).as("user_name"),
                                bigInt(user().getTableName(), user().getColumnName(SERVER)).as("server_id"),
                                timestamp(user().getTableName(), user().getColumnName(CREATION)).as("user_creation"),

                                bigInt(account().getTableName(), account().getColumnName(IDENTIFIER)).as("account_id"),
                                string(account().getTableName(), account().getColumnName(PASSWORD)).as("account_password"),
                                string(account().getTableName(), account().getColumnName(PIN)).as("account_pin"),
                                string(account().getTableName(), account().getColumnName(TOTP)).as("account_totp"),
                                timestamp(account().getTableName(), account().getColumnName(TERMINATION)).as("account_termination"),
                                string(account().getTableName(), account().getColumnName(TERMINATOR)).as("account_terminator"),
                                timestamp(account().getTableName(), account().getColumnName(CREATION)).as("account_creation"),

                                bigInt(session().getTableName(), session().getColumnName(IDENTIFIER)).as("session_id"),
                                bool(session().getTableName(), session().getColumnName(PASSWORD)).as("session_password"),
                                bool(session().getTableName(), session().getColumnName(PIN)).as("session_pin"),
                                bool(session().getTableName(), session().getColumnName(TOTP)).as("session_totp"),
                                timestamp(session().getTableName(), session().getColumnName(VALIDATION)).as("session_validation"),

                                string(uuid().getTableName(), uuid().getColumnName(OFFLINE)).as("uuid_offline"),
                                string(uuid().getTableName(), uuid().getColumnName(ONLINE)).as("uuid_online"),
                                string(uuid().getTableName(), uuid().getColumnName(BEDROCK)).as("uuid_bedrock")
                        ).from(DSL.table(user().getTableName()))
                        .join(account().getTableName())
                        .on(bigInt(user().getTableName(), user().getColumnName(ACCOUNT))
                                .eq(bigInt(account().getTableName(), account().getColumnName(IDENTIFIER))))
                        .join(session().getTableName())
                        .on(bigInt(user().getTableName(), user().getColumnName(SESSION))
                                .eq(bigInt(session().getTableName(), session().getColumnName(IDENTIFIER))))
                        .join(uuid().getTableName())
                        .on(bigInt(user().getTableName(), user().getColumnName(UUID))
                                .eq(bigInt(uuid().getTableName(), uuid().getColumnName(IDENTIFIER))));

                Result<Record19<ULong, String, ULong, Timestamp,
                        ULong, String, String, String, Timestamp, String, Timestamp,
                        ULong, Boolean, Boolean, Boolean, Timestamp,
                        String, String, String>> result = step.fetch();

                for (Record19<ULong, String, ULong, Timestamp,
                        ULong, String, String, String, Timestamp, String, Timestamp,
                        ULong, Boolean, Boolean, Boolean, Timestamp,
                        String, String, String> resultRecord : result) {

                    UnsignedBigInteger userId = new UnsignedBigInteger(resultRecord.get("user_id", ULong.class));
                    String username = resultRecord.get("user_name", String.class);
                    UnsignedBigInteger serverId = new UnsignedBigInteger(resultRecord.get("server_id", ULong.class));
                    Instant userCreation = resultRecord.get("user_creation", Timestamp.class).toInstant();

                    UnsignedBigInteger accountId = new UnsignedBigInteger(resultRecord.get("account_id", ULong.class));
                    String accountPassword = resultRecord.get("account_password", String.class);
                    String accountPin = resultRecord.get("account_pin", String.class);
                    String accountTotp = resultRecord.get("account_totp", String.class);
                    Timestamp accountTerminationTimestamp = resultRecord.get("account_termination", Timestamp.class);
                    Instant accountTermination = (accountTerminationTimestamp == null ? null : accountTerminationTimestamp.toInstant());

                    String accountTerminator = resultRecord.get("account_terminator", String.class);
                    Instant accountCreation = resultRecord.get("account_creation", Timestamp.class).toInstant();

                    UnsignedBigInteger sessionId = new UnsignedBigInteger(resultRecord.get("session_id", ULong.class));
                    boolean sessionPassword = resultRecord.get("session_password", Boolean.class);
                    boolean sessionPin = resultRecord.get("session_pin", Boolean.class);
                    boolean sessionTotp = resultRecord.get("session_totp", Boolean.class);
                    Instant sessionValidation = resultRecord.get("session_validation", Timestamp.class).toInstant();

                    String uuidOffline = resultRecord.get("uuid_offline", String.class);
                    String uuidOnline = resultRecord.get("uuid_online", String.class);
                    String uuidBedrock = resultRecord.get("uuid_bedrock", String.class);

                    Account account = AAccount.builder(accountId, userId)
                            .password(accountPassword)
                            .pin(accountPin)
                            .totp(accountTotp)
                            .terminated(accountTerminator, accountTermination)
                            .creation(accountCreation)
                            .build(this.connector);

                    Session session = new SSession(sessionId, userId, this.connector,
                            sessionPassword, sessionPin, sessionTotp, sessionValidation);

                    assert plugin != null;
                    SServer server = (SServer) plugin.getServerManager().getServer(serverId);

                    UUIDStore store = UUIDStore.compose(uuidOffline, uuidOnline, uuidBedrock);

                    OfflineClient client = OOfflineClient.createOfflineBuilder(this.connector)
                            .id(userId)
                            .name(username)
                            .uuid(store)
                            .account(account)
                            .session(session)
                            .creation(userCreation)
                            .server(server)
                            .build();

                    server.addOfflineClient(client);
                    clients.add(client);
                }
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }

            return clients;
        }, this.connector.getExecutor());
    }

    public CompletableFuture<OfflineClient> setupAccount(final @NotNull String name, final @NotNull UUID offline, final @Nullable UUID online,
                                                         final @Nullable UUID bedrock, final SServer server) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = connector.getContext();
            try {
                Record1<ULong> id = context
                        .select(
                                bigInt(user().getTableName(), user().getColumnName(IDENTIFIER))
                        )
                        .from(DSL.table(user().getTableName()))
                        .innerJoin(DSL.table(uuid().getTableName()))
                        .on(bigInt(user().getTableName(), user().getColumnName(UUID))
                                .equal(bigInt(uuid().getTableName(), uuid().getColumnName(IDENTIFIER))))
                        .where(string(uuid().getTableName(), uuid().getColumnName(OFFLINE))
                                .equal(offline.toString()))
                        .or(string(uuid().getTableName(), uuid().getColumnName(ONLINE))
                                .equal(String.valueOf(online)))
                        .or(string(uuid().getTableName(), uuid().getColumnName(BEDROCK))
                                .equal(String.valueOf(bedrock)))
                        .fetchAny();

                if (id != null && id.value1() != null)
                    return null;

                CompletableFuture<CreationCompletionResult> setupResult = this.setupUserData(name, offline, online, bedrock, server);
                return setupResult.thenCompose(result -> {
                    UUIDStore store = new UUIDStore(offline, online, bedrock);
                    AAccount account = AAccount.builder(result.createdAccountId, result.createdUserId)
                            .creation(result.createdAccountTime)
                            .build(this.connector);
                    SSession session = new SSession(result.createdSessionId, result.createdUserId, this.connector, false, false, false,
                            result.createdSessionTime);

                    OfflineClient client = OOfflineClient.createOfflineBuilder(this.connector)
                            .id(result.createdUserId)
                            .name(name)
                            .uuid(store)
                            .account(account)
                            .session(session)
                            .creation(result.createdUserTime)
                            .server(server)
                            .build();

                    server.addOfflineClient(client);
                    return CompletableFuture.completedFuture(client);
                }).exceptionally(error -> {
                    throw new SQLDaoException(error);
                });
            } catch (DataAccessException | SQLException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor())
                .thenCompose(future -> future);
    }

    private CompletableFuture<CreationCompletionResult> setupUserData(final @NotNull String name, final @NotNull UUID offline,
                                                                      final @Nullable UUID online, final @Nullable UUID bedrock,
                                                                      final @NotNull SServer server) throws SQLException {
        return this.connector.createTransaction().transactionResultAsync(this.connector.getExecutor(), configuration -> {
            DSLContext transaction = DSL.using(configuration);
            try {
                InsertResultStep<Record2<ULong, Timestamp>> step1 = transaction.insertInto(DSL.table(account().getTableName()))
                        .defaultValues()
                        .returningResult(
                                DSL.field(account().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED),
                                DSL.field(account().getColumnName(CREATION), SQLDataType.TIMESTAMP)
                        );
                Record accountIdRecord = step1.fetchSingle();

                InsertResultStep<Record2<ULong, Timestamp>> step2 = transaction.insertInto(DSL.table(session().getTableName()))
                        .defaultValues()
                        .returningResult(
                                DSL.field(session().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED),
                                DSL.field(session().getColumnName(VALIDATION), SQLDataType.TIMESTAMP)
                        );
                Record sessionIdRecord = step2.fetchSingle();

                InsertResultStep<Record1<ULong>> step3 = transaction.insertInto(DSL.table(uuid().getTableName()))
                        .columns(
                                DSL.field(uuid().getColumnName(OFFLINE)),
                                DSL.field(uuid().getColumnName(ONLINE)),
                                DSL.field(uuid().getColumnName(BEDROCK))
                        ).values(offline, online, bedrock)
                        .returningResult(
                                DSL.field(uuid().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED)
                        );
                Record uuidIdRecord = step3.fetchSingle();

                ULong accountId = accountIdRecord.get(DSL.field(account().getColumnName(IDENTIFIER), ULong.class));
                Instant accountCreation = accountIdRecord.get(DSL.field(account().getColumnName(CREATION), Timestamp.class))
                        .toInstant();

                ULong sessionId = sessionIdRecord.get(DSL.field(session().getColumnName(IDENTIFIER), ULong.class));
                Instant sessionValidation = sessionIdRecord.get(DSL.field(session().getColumnName(VALIDATION)), Timestamp.class)
                        .toInstant();

                ULong uuidId = uuidIdRecord.get(DSL.field(uuid().getColumnName(IDENTIFIER), ULong.class));

                CreationCompletionResult result = new CreationCompletionResult();
                result.createdAccountId = new UnsignedBigInteger(accountId);
                result.createdAccountTime = accountCreation;

                result.createdSessionId = new UnsignedBigInteger(sessionId);
                result.createdSessionTime = sessionValidation;

                Record executionResult = transaction.insertInto(DSL.table(user().getTableName()))
                        .columns(DSL.field(user().getColumnName(USERNAME)),
                                DSL.field(user().getColumnName(ACCOUNT)),
                                DSL.field(user().getColumnName(SESSION)),
                                DSL.field(user().getColumnName(SERVER)),
                                DSL.field(user().getColumnName(UUID))
                        ).values(name,
                                accountId,
                                sessionId,
                                ULong.valueOf(server.getEntityId()),
                                uuidId
                        )
                        .returning(
                                DSL.field(user().getColumnName(IDENTIFIER)),
                                DSL.field(user().getColumnName(CREATION))
                        )
                        .fetchSingle();

                result.createdUserId = new UnsignedBigInteger(executionResult.get(DSL.field(user().getColumnName(IDENTIFIER), Long.class)));
                result.createdUserTime = executionResult.get(DSL.field(user().getColumnName(CREATION)), Timestamp.class).toInstant();

                return result;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }).toCompletableFuture();
    }
    
    private Field<ULong> bigInt(final String table, final String field) {
        return field(table, field, SQLDataType.BIGINTUNSIGNED);
    }

    private Field<String> string(final String table, final String field) {
        return field(table, field, SQLDataType.VARCHAR);
    }

    private Field<Boolean> bool(final String table, final String field) {
        return field(table, field, SQLDataType.BOOLEAN);
    }

    private Field<Timestamp> timestamp(final String table, final String field) {
        return field(table, field, SQLDataType.TIMESTAMP);
    }

    private <T> Field<T> field(final String table, final String field, final DataType<T> type) {
        return DSL.field(String.format("%s.%s", table, field), type);
    }
    
    private static class CreationCompletionResult {
        private UnsignedBigInteger createdUserId;
        private Instant createdUserTime;

        private UnsignedBigInteger createdAccountId;
        private Instant createdAccountTime;

        private UnsignedBigInteger createdSessionId;
        private Instant createdSessionTime;
    }
}
