/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.sequel.connector.schema;

import es.karmadev.api.kson.JsonInstance;
import es.karmadev.api.kson.JsonObject;
import es.karmadev.api.kson.KsonException;
import es.karmadev.api.kson.io.JsonReader;
import es.karmadev.api.kson.io.JsonWriter;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.sequel.model.column.SQColumn;
import es.karmadev.authlock.api.sequel.model.table.SQTable;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;

import static es.karmadev.authlock.api.sequel.model.table.SQTable.*;

public final class SchemaStorage {

    private static final String TABLE = "table";
    private final AuthLock plugin;

    public SchemaStorage(final AuthLock plugin) {
        this.plugin = plugin;
    }

    public Collection<RenameData> detectUserSchema() throws SchemaException {
        return makeSchema(user());
    }

    public Collection<RenameData> detectServerSchema() throws SchemaException {
        return makeSchema(server());
    }

    public Collection<RenameData> detectSessionSchema() throws SchemaException {
        return makeSchema(session());
    }

    public Collection<RenameData> detectAccountSchema() throws SchemaException {
        return makeSchema(account());
    }

    public Collection<RenameData> detectUUIDSchema() throws SchemaException {
        return makeSchema(uuid());
    }

    public void updateUserSchema() {
        saveSchema(user());
    }

    public void updateServerSchema() {
        saveSchema(server());
    }

    public void updateSessionSchema() {
        saveSchema(session());
    }

    public void updateAccountSchema() {
        saveSchema(account());
    }

    public void updateUUIDSchema() {
        saveSchema(uuid());
    }

    private Collection<RenameData> makeSchema(final SQTable table) {
        Path schemaData = createSchemaPath(table);

        if (!Files.exists(schemaData)) {
            saveSchema(table, schemaData);
            return Collections.emptySet();
        } else {
            JsonObject currentSchema = loadSchema(schemaData);
            return mapColumnConversions(table, currentSchema);
        }
    }

    private void saveSchema(final SQTable table) {
        Path schemaData = createSchemaPath(table);

        saveSchema(table, schemaData);
    }

    private static void saveSchema(final SQTable table, final Path schemaData) {
        JsonObject currentSchema = JsonObject.newObject("", "");
        currentSchema.put(TABLE, table.getTableName());
        for (SQColumn column : table.getColumns())
            currentSchema.put(column.getName(), table.getColumnName(column));

        saveJsonSchema(schemaData, currentSchema);
    }

    private static Collection<RenameData> mapColumnConversions(final SQTable table, final JsonObject schema) {
        Collection<RenameData> renameData = new HashSet<>();
        detectTableName(table, schema, renameData);

        for (SQColumn column : table.getColumns()) {
            JsonInstance activeInstance = schema.getChild(column.getName());
            if (!activeInstance.isNativeType() || activeInstance.asString() == null) continue;

            String current = activeInstance.asString();
            assert current != null;

            String expected = table.getColumnName(column);
            if (!current.equals(expected))
                renameData.add(RenameData.make(current, expected, false));
        }

        return renameData;
    }

    private static void detectTableName(final SQTable table, final JsonObject schema, final Collection<RenameData> output) {
        JsonInstance activeInstance = schema.getChild(TABLE);
        if (!activeInstance.isNativeType() || activeInstance.asString() == null) return;

        String current = activeInstance.asString();
        assert current != null;

        String expected = table.getTableName();
        if (!current.equals(expected))
            output.add(RenameData.make(current, expected, true));
    }

    private static JsonObject loadSchema(final Path schemaData) throws SchemaException {
        try (Reader reader = Files.newBufferedReader(schemaData)) {
            JsonInstance instance = JsonReader.read(reader);
            if (instance == null || !instance.isObjectType())
                throw new SchemaException("Invalid schema for user");

            return instance.asObject();
        } catch (IOException ex) {
            throw new SchemaException(ex);
        }
    }

    private static void saveJsonSchema(Path schemaData, JsonObject currentSchema) {
        Path parent = schemaData.getParent();
        if (!Files.exists(parent))
            try {
                Files.createDirectories(parent);
            } catch (IOException ex) {
                throw new SchemaException(ex);
            }

        try(Writer writer = Files.newBufferedWriter(schemaData, StandardOpenOption.CREATE)) {
            JsonWriter jWriter = new JsonWriter(currentSchema);
            jWriter.export(writer);
        } catch (IOException | KsonException ex) {
            throw new SchemaException(ex);
        }
    }

    private @NotNull Path createSchemaPath(final SQTable table) {
        return plugin.getDataFolder().resolve(TABLE)
                .resolve("schema")
                .resolve(table.getName())
                .resolve("schema.json");
    }
}