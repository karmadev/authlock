/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client.instance.offline;

import es.karmadev.authlock.api.authority.AuthorityContext;
import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.player.client.instance.authority.ClientAuthority;
import es.karmadev.authlock.common.player.client.instance.online.OOnlineClient;
import es.karmadev.authlock.common.player.client.instance.online.OnlineClientBridge;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServer;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

public class OOfflineClient implements OfflineClient {

    private static final AuthLock plugin = AuthLock.getInstance();

    private final UnsignedBigInteger id;
    private final Account account;
    private final Session session;
    private final Instant creation;

    private final ClientAuthority authority = new ClientAuthority(this);
    private final DatabaseConnector connector;
    private final OfflineClientDAO dao;

    private OnlineClient onlineClient = null;
    protected String name;
    protected UUIDStore uuidStore;
    protected SServer server;

    protected OOfflineClient(final UnsignedBigInteger id, final Account account, final Session session, final Instant creation,
                           final DatabaseConnector connector) {
        if (plugin == null)
            throw new InitializationRequiredException(OOfflineClient.class,
                    "OOfflineClient(UnsignedBigInteger, String, UUIDStore, Account, Session, Server, Instant, DatabaseConnector)");

        Preconditions.assertNotNull(id, "id");
        Preconditions.assertNotNull(account, "account");
        Preconditions.assertNotNull(session, "session");
        Preconditions.assertNotNull(creation, "creation");
        Preconditions.assertNotNull(connector, "connector");

        this.id = id;
        this.account = account;
        this.session = session;
        this.creation = creation;

        this.connector = connector;
        this.dao = new OfflineClientDAO(connector);
    }

    /**
     * Get the client ID
     *
     * @return the client ID
     */
    @Override
    public @NotNull UnsignedBigInteger getEntityId() {
        return this.id;
    }

    /**
     * Get the client name
     *
     * @return the client name
     */
    @Override
    public @NotNull String getName() {
        return this.name;
    }

    /**
     * Set the account name
     *
     * @param name the account name
     * @return if the action was successful
     */
    @Override @Contract("null -> fail")
    public @NotNull CompletableFuture<Boolean> setName(final String name) {
        if (name == null || name.isEmpty())
            return CompletableFuture.completedFuture(false);

        return this.dao.updateName(this.id, name)
                .thenApply(ignored -> {
                    this.name = name;
                    return true;
                })
                .exceptionally(error -> {
                    assert plugin != null;

                    plugin.getLogger().error(error, "Failed to update username");
                    return false;
                });
    }

    /**
     * Get the client UUID store
     *
     * @return the UUID store
     */
    @Override
    public @NotNull UUIDStore getUUID() {
        return this.uuidStore;
    }

    /**
     * Set the account UUID data
     *
     * @param uuid the uuid data
     * @return if the action was successful
     */
    @Override @Contract("null -> fail")
    public @NotNull CompletableFuture<Boolean> setUUID(final UUIDStore uuid) {
        if (uuid.equals(this.uuidStore))
            return CompletableFuture.completedFuture(false);

        return this.dao.updateUUID(this.id, uuid)
                .thenApply(ignored -> {
                    this.uuidStore = uuid;
                    return true;
                })
                .exceptionally(error -> {
                    assert plugin != null;

                    plugin.getLogger().error(error, "Failed to update uuid");
                    return false;
                });
    }

    /**
     * Get the online client of this
     * offline client
     *
     * @return the online client
     */
    @Override
    public @Nullable OnlineClient getOnlineClient() {
        assert plugin != null;

        return Optional.ofNullable(this.onlineClient).orElseGet(() -> {
            this.onlineClient = plugin.getClientManager().getClient(this.id);
            return this.onlineClient;
        });
    }

    /**
     * Get the client account
     *
     * @return the client account
     */
    @Override
    public @NotNull Account getAccount() {
        return this.account;
    }

    /**
     * Get the client session
     *
     * @return the client session
     */
    @Override
    public @NotNull Session getSession() {
        return this.session;
    }

    /**
     * Get the client server
     *
     * @return the client server
     */
    @Override
    public Server getServer() {
        return this.server;
    }

    /**
     * Set the client server
     *
     * @param server the new server
     * @return if the action was successful
     */
    @Override
    public CompletableFuture<Boolean> setServer(final Server server) {
        if (server == null || server.equals(this.server))
            return CompletableFuture.completedFuture(false);

        if (!(server instanceof SServer))
            throw new UnsupportedOperationException("Cannot apply server " + server + " to client");

        SServer serverToApply = (SServer) server;
        return this.dao.updateServer(this.id, serverToApply.getEntityId())
                .thenApply(ignored -> {
                    this.server.removeOfflineClient(this);
                    serverToApply.addOfflineClient(this);

                    this.server = serverToApply;
                    return true;
                })
                .exceptionally(error -> {
                    assert plugin != null;

                    plugin.getLogger().error(error, "Failed to update uuid");
                    return false;
                });
    }

    /**
     * Get if the client is
     * currently connected
     *
     * @return if the client is
     * connected
     */
    @Override
    public boolean isConnected() {
        OnlineClient existingClient = getOnlineClient();
        return existingClient != null &&
                existingClient.isConnected();
    }

    /**
     * Get when the client was created
     * in the database
     *
     * @return when the client was created
     */
    @Override
    public Instant getCreation() {
        return this.creation;
    }

    /**
     * Get the authority level
     *
     * @return the authority level
     */
    @Override
    public String getAuthorityName() {
        return this.name;
    }

    /**
     * Get the authority entity
     * context
     *
     * @return the context
     */
    @Override
    public AuthorityContext getAuthorityContext() {
        return this.authority;
    }

    /**
     * Get the client as an online
     * client
     *
     * @param bridge the client bridge
     * @return the online client
     */
    public OOnlineClient toOnline(final OnlineClientBridge bridge) {
        OnlineClient existing = this.getOnlineClient();
        if (existing instanceof OOnlineClient)
            return (OOnlineClient) existing;

        return OOnlineClient.createOnlineBuilder(this.connector)
                .id(id)
                .name(this.name)
                .uuid(this.uuidStore)
                .account(this.account)
                .session(this.session)
                .server(this.server)
                .creation(this.creation)
                .bridge(bridge)
                .build();
    }

    public static OfflineClientBuilder createOfflineBuilder(final DatabaseConnector connector) {
        return new OfflineClientBuilder(connector);
    }

    public static class OfflineClientBuilder {

        protected final DatabaseConnector connector;

        protected UnsignedBigInteger id;
        protected String name;
        protected UUIDStore uuidStore;
        protected Account account;
        protected Session session;
        protected SServer server;
        protected Instant creation;

        protected OfflineClientBuilder(final DatabaseConnector connector) {
            //Prevent external instantiation
            this.connector = connector;
        }

        public OfflineClientBuilder id(final UnsignedBigInteger id) {
            this.id = id;
            return this;
        }

        public OfflineClientBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public OfflineClientBuilder uuid(final UUIDStore store) {
            this.uuidStore = store;
            return this;
        }

        public OfflineClientBuilder account(final Account account) {
            this.account = account;
            return this;
        }

        public OfflineClientBuilder session(final Session session) {
            this.session = session;
            return this;
        }

        public OfflineClientBuilder server(final SServer server) {
            this.server = server;
            return this;
        }

        public OfflineClientBuilder creation(final Instant creation) {
            this.creation = creation;
            return this;
        }

        public OOfflineClient build() {
            Preconditions.assertNotNull(name, "name");
            Preconditions.assertNotNull(uuidStore, "uuidStore");
            Preconditions.assertNotNull(server, "server");

            OOfflineClient client = new OOfflineClient(this.id, this.account, this.session, this.creation, this.connector);
            client.name = name;
            client.uuidStore = uuidStore;
            client.server = server;

            return client;
        }
    }
}
