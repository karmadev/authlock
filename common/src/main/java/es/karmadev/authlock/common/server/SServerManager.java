/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.server;

import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.server.ServerManager;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public class SServerManager implements ServerManager {

    private static final AuthLock plugin = AuthLock.getInstance();

    private static final String UPDATE_ERROR_MESSAGE = "Failed to update server {}";
    private static final String INSERT_ERROR_MESSAGE = "Failed to create server {}";
    private static final String CACHE_ERROR_MESSAGE = "Failed to prepare server manager cache";

    private final Collection<Server> servers = ConcurrentHashMap.newKeySet();
    private final ServerManagerDAO dao;

    public SServerManager(final ServerBridge bridge, final DatabaseConnector connector) {
        if (plugin == null)
            throw new InitializationRequiredException(SServerManager.class, "SServerManager(DatabaseConnector)");

        this.dao = new ServerManagerDAO(bridge, connector);
    }

    /**
     * Creates a new server or updates
     * an existing one
     *
     * @param name    the server name
     * @param address the server address
     * @param port    the server port
     * @return the updated server
     */
    @Override
    public CompletableFuture<Server> insertOrUpdateServer(final String name, final InetAddress address, final int port) {
        Server named = this.getServer(name);
        Server addressed = this.getServer(address, port);

        if (named != null || addressed != null) {
            Server toAlter = (named == null ? addressed : named);
            return this.dao.updateServer(toAlter.getEntityId(), name, address, port)
                    .thenApply(server -> {
                        this.servers.remove(toAlter);
                        this.servers.add(server);

                        return server;
                    })
                    .exceptionally(error -> {
                        assert plugin != null;
                        if (error == null) {
                            plugin.getLogger().severe(UPDATE_ERROR_MESSAGE, name);
                            return null;
                        }

                        plugin.getLogger().error(error, UPDATE_ERROR_MESSAGE, name);
                        return null;
                    });
        }

        return this.dao.insertServer(name, address, port)
                .thenApply(server -> {
                    this.servers.add(server);
                    return server;
                })
                .exceptionally(error -> {
                    assert plugin != null;
                    if (error == null) {
                        plugin.getLogger().severe(INSERT_ERROR_MESSAGE, name);
                        return null;
                    }

                    plugin.getLogger().error(error, INSERT_ERROR_MESSAGE, name);
                    return null;
                });
    }

    /**
     * Get all the existing servers
     *
     * @return the existing server
     */
    @Override
    public Collection<Server> getServers() {
        return Collections.unmodifiableCollection(this.servers);
    }

    /**
     * Prepares the object manager
     * cache
     */
    @Override
    public @NotNull CompletableFuture<Void> prepareCache() {
        return this.dao.getAllServers().thenAccept(result -> {
            this.servers.clear();
            this.servers.addAll(result);
        }).exceptionally(error -> {
            assert plugin != null;
            if (error == null) {
                plugin.getLogger().severe(CACHE_ERROR_MESSAGE);
                return null;
            }

            plugin.getLogger().error(error, CACHE_ERROR_MESSAGE);
            return null;
        });
    }
}
