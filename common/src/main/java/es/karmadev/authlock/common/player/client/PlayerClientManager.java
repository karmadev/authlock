/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.client.ClientManager;
import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.UnsignedException;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.player.client.instance.offline.OOfflineClient;
import es.karmadev.authlock.common.player.client.instance.online.OnlineClientBridge;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServer;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public class PlayerClientManager implements ClientManager {

    private static final AuthLock plugin = AuthLock.getInstance();

    private static boolean filterByUUID(final OfflineClient client, final UUIDStore store) {
        UUIDStore uuid = client.getUUID();
        return store.containsAny(uuid);
    }

    private final ClientManagerDAO dao;
    private final Set<OfflineClient> offlineClients = new HashSet<>();
    private final Set<OnlineClient> onlineClients = ConcurrentHashMap.newKeySet();

    public PlayerClientManager(final DatabaseConnector connector) {
        if (plugin == null)
            throw new InitializationRequiredException(OOfflineClient.class, "CClientManager(DatabaseConnector)");
        this.dao = new ClientManagerDAO(connector);
    }

    /**
     * Create a client
     *
     * @param name the client name
     * @param online the online uuid
     * @param bedrock the bedrock UUID
     * @param server the client server
     * @return if the client was created successfully
     */
    @Override
    public @NotNull CompletableFuture<Boolean> createClient(final @NotNull String name, final @Nullable UUID online, final @Nullable UUID bedrock,
                                                            final @NotNull Server server) {
        Preconditions.assertNotNull(name, "name");
        Preconditions.assertNotNull(server, "server");

        if (!(server instanceof SServer))
            throw new UnsupportedOperationException("Cannot create client with server " + server);

        UUID offline = this.offlineId(name);
        return this.dao.setupAccount(name, offline, online, bedrock, (SServer) server)
                .thenCompose(client -> {
                    this.offlineClients.add(client);
                    return CompletableFuture.completedFuture(true);
                });
    }

    /**
     * Get all the offline clients
     *
     * @return the offline clients
     */
    @Override
    public Collection<OfflineClient> getOfflineClients() {
        return Collections.unmodifiableCollection(this.offlineClients);
    }

    /**
     * Get all the online clients
     *
     * @return the online clients
     */
    @Override
    public Collection<OnlineClient> getOnlineClients() {
        return Collections.unmodifiableCollection(this.onlineClients);
    }

    /**
     * Get an offline client
     *
     * @param uuid the client uuid
     * @return the offline client
     */
    @Override
    public @Nullable OfflineClient getOfflineClient(final @NotNull UUIDStore uuid) {
        return this.getOfflineClients().stream()
                .filter(c -> filterByUUID(c, uuid))
                .findAny()
                .orElse(null);
    }

    /**
     * Get an offline client
     *
     * @param name the client name
     * @return the offline client
     */
    @Override
    public @Nullable OfflineClient getOfflineClient(final @NotNull String name) {
        return this.getOfflineClients().stream()
                .filter(c -> c.getName().equals(name))
                .findAny()
                .orElse(null);
    }

    /**
     * Get an offline client
     *
     * @param clientId the client id
     * @return the client
     * @throws UnsignedException if the number is signed
     */
    @Override
    public @Nullable OfflineClient getOfflineClient(final @NotNull Number clientId) throws UnsignedException {
        UnsignedBigInteger unsignedId = new UnsignedBigInteger(clientId);
        return this.getOfflineClients().stream()
                .filter(c -> c.getEntityId().equals(unsignedId))
                .findAny()
                .orElse(null);
    }

    public void connect(final OOfflineClient client, final OnlineClientBridge bridge) {
        if (client == null || client.isConnected())
            return;
        
        this.onlineClients.add(client.toOnline(bridge));
    }
    
    public void disconnect(final OnlineClient client) {
        if (client == null || !client.isConnected())
            return;
        
        this.onlineClients.remove(client);
    }

    private UUID offlineId(final String name) {
        return UUID.nameUUIDFromBytes(("OfflinePlayer:" + name).getBytes());
    }

    /**
     * Prepares the object manager
     * cache
     */
    @Override
    public @NotNull CompletableFuture<Void> prepareCache() {
        return dao.getAllClients().thenAccept(this.offlineClients::addAll)
                .exceptionally(error -> {
                    assert plugin != null;

                    plugin.getLogger().error(error, "Failed to cache clients");
                    return null;
                });
    }
}
