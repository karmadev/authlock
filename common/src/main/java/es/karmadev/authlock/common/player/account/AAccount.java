/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.account;

import es.karmadev.authlock.api.authority.AuthorityContext;
import es.karmadev.authlock.api.authority.AuthorityEntity;
import es.karmadev.authlock.api.authority.exception.AuthorizationException;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jetbrains.annotations.NotNull;
import org.jooq.tools.StringUtils;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;
import java.util.function.Consumer;
import java.util.function.Function;

public final class AAccount implements Account {

    private static final AuthLock plugin = AuthLock.getInstance();

    private final UnsignedBigInteger id;
    private final UnsignedBigInteger clientId;
    private final AccountDAO dao;

    private String password;
    private String pin;
    private String totp;
    private String terminator;
    private Instant termination;
    private Instant creation;

    private AAccount(final @NotNull UnsignedBigInteger id, final @NotNull UnsignedBigInteger clientId,
                     final @NotNull DatabaseConnector connector) throws InitializationRequiredException {
        if (plugin == null)
            throw new InitializationRequiredException(AAccount.class, "AAccount(UnsignedBigInteger, UnsignedBigInteger, DatabaseConnector)");

        Preconditions.assertNotNull(id, "id");
        Preconditions.assertNotNull(clientId, "clientId");
        Preconditions.assertNotNull(connector, "connector");

        this.id = id;
        this.clientId = clientId;
        this.dao = new AccountDAO(connector);
    }

    /**
     * Get the SQL entity id
     *
     * @return the entity id
     */
    @Override
    public @NotNull UnsignedBigInteger getEntityId() {
        return this.id;
    }

    /**
     * Get the client of the account
     *
     * @return the account client
     */
    @Override
    public UnsignedBigInteger getClientId() {
        return this.clientId;
    }

    /**
     * Get the account password
     *
     * @return the account password
     */
    @Override
    public @NotNull String getPassword() {
        return Preconditions.getOrElse(this.password, Preconditions.EMPTY_STR);
    }

    /**
     * Get if the account has a
     * password
     *
     * @return if the account has the
     * password defined
     */
    @Override
    public boolean hasPassword() {
        return !StringUtils.isBlank(this.password);
    }

    /**
     * Set the account password
     *
     * @param newPassword the new account password
     * @return if the password was set
     */
    @Override
    public CompletableFuture<Boolean> setPassword(final @NotNull String newPassword) {
        return this.updateField(newPassword,
                value -> this.password = value,
                value -> dao.updatePassword(this.id, value));
    }

    /**
     * Get the account pin
     *
     * @return the account pin
     */
    @Override
    public @NotNull String getPin() {
        return Preconditions.getOrElse(this.pin, Preconditions.EMPTY_STR);
    }

    /**
     * Get if the account has a
     * pin
     *
     * @return if the account has a pin
     */
    @Override
    public boolean hasPin() {
        return !StringUtils.isBlank(this.pin);
    }

    /**
     * Set the account pin
     *
     * @param newPin the new pin
     * @return if the pin was set
     */
    @Override
    public CompletableFuture<Boolean> setPin(final @NotNull String newPin) {
        return this.updateField(newPin,
                value -> this.pin = value,
                value -> dao.updatePin(this.id, value));
    }

    /**
     * Get the client totp token
     *
     * @return the client totp token
     */
    @Override
    public @NotNull String getTotpToken() {
        return Preconditions.getOrElse(this.totp, Preconditions.EMPTY_STR);
    }

    /**
     * Get if the client has the
     * totp token defined
     *
     * @return if the client has a totp
     * token
     */
    @Override
    public boolean hasTotpToken() {
        return !StringUtils.isBlank(this.totp);
    }

    /**
     * Set the account totp token
     *
     * @param newTotpToken the new totp
     *                     token
     * @return if the token was set
     */
    @Override
    public CompletableFuture<Boolean> setTotpToken(final @NotNull String newTotpToken) {
        return this.updateField(newTotpToken,
                value -> this.totp = value,
                value -> dao.updateTotp(this.id, value));
    }

    /**
     * Delete the account
     *
     * @param authority the account termination
     *                  issuer
     * @return if the account was successfully
     * removed
     * @throws AuthorizationException if the authority does
     *                                not have power over the account
     */
    @Override
    public CompletableFuture<Boolean> terminate(final @NotNull AuthorityEntity authority) throws AuthorizationException {
        AuthorityContext context = authority.getAuthorityContext();
        if (!context.appliesTo(this))
            throw new AuthorizationException(authority, this);

        if (this.wasTerminated())
            return CompletableFuture.completedFuture(false);

        return this.dao.terminate(this.id, authority).thenApply(result -> {
            if (result == null)
                return false;

            this.terminator = authority.getAuthorityName();
            this.termination = result;
            return true;
        }).exceptionally(error -> {
            assert plugin != null;

            plugin.getLogger().error(error, "Failed to terminate account");
            return false;
        });
    }

    /**
     * Get if the account was terminated
     *
     * @return if the account was
     * terminated
     */
    @Override
    public boolean wasTerminated() {
        return !StringUtils.isBlank(this.terminator) && this.termination != null;
    }

    /**
     * Get the account terminator
     *
     * @return the terminator
     * of the account
     */
    @Override
    public @NotNull String getTerminator() {
        return Preconditions.getOrElse(this.terminator, Preconditions.EMPTY_STR);
    }

    /**
     * Get when the client account
     * was created
     *
     * @return the account creation time
     */
    @Override
    public @NotNull Instant getCreationDate() {
        return this.creation;
    }

    /**
     * Get when the client account
     * was terminated
     *
     * @return the account termination time
     */
    @Override
    public Instant getTerminationDate() {
        return this.termination;
    }

    private CompletableFuture<Boolean> updateField(final String newValue, final Consumer<String> setter,
                                                   final Function<String, CompletableFuture<Boolean>> updater) {
        String vToSet = Preconditions.getOrElse(newValue, Preconditions.EMPTY_STR).trim();
        return updater.apply(vToSet).thenApply(success -> {
            if (Boolean.TRUE.equals(success))
                setter.accept(vToSet);

            return success;
        }).exceptionally(error -> {
            assert plugin != null;

            plugin.getLogger().error(error, "Failed to update account field");
            return false;
        });
    }

    public static Builder builder(final @NotNull UnsignedBigInteger id, final @NotNull UnsignedBigInteger clientId) {
        return new Builder(id, clientId);
    }

    public static class Builder {

        private final UnsignedBigInteger id;
        private final UnsignedBigInteger clientId;

        private String password;
        private String pin;
        private String totp;
        private String terminator;
        private Instant termination;
        private Instant creation;

        private Builder(final @NotNull UnsignedBigInteger id, final @NotNull UnsignedBigInteger clientId) {
            Preconditions.assertNotNull(id, "id");
            Preconditions.assertNotNull(clientId, "clientId");

            this.id = id;
            this.clientId = id;
        }

        public Builder password(final String password) {
            this.password = Preconditions.getOrElse(password, Preconditions.EMPTY_STR).trim();
            return this;
        }

        public Builder pin(final String pin) {
            this.pin = Preconditions.getOrElse(pin, Preconditions.EMPTY_STR).trim();
            return this;
        }

        public Builder totp(final String totp) {
            this.totp = Preconditions.getOrElse(totp, Preconditions.EMPTY_STR).trim();
            return this;
        }

        public Builder terminated(final String terminator, final Instant termination) {
            assertCombination(terminator, termination);

            this.terminator = Preconditions.getOrElse(terminator, Preconditions.EMPTY_STR).trim();
            this.termination = termination;
            return this;
        }

        public Builder creation(final @NotNull Instant creation) {
            Preconditions.assertNotNull(creation, "creation");
            
            this.creation = creation;
            return this;
        }

        /**
         * Build the account
         *
         * @param connector the database connector
         * @return the account
         * @throws InitializationRequiredException if the authlock plugin has not
         * been yet initialized
         */
        public AAccount build(final @NotNull DatabaseConnector connector) throws InitializationRequiredException {
            Preconditions.assertNotNull(connector, "connector");
            Preconditions.assertNotNull(creation, "creation");

            AAccount account = new AAccount(this.id, this.clientId, connector);
            account.password = this.password;
            account.pin = this.pin;
            account.totp = this.totp;
            account.terminator = this.terminator;
            account.termination = this.termination;
            account.creation = this.creation;

            return account;
        }

        private static void assertCombination(final String terminator, final Instant termination) {
            if ((terminator == null || StringUtils.isBlank(terminator)) &&
                    termination != null) {
                throw new AssertionError("Cannot proceed with terminated account. Invalid terminator and termination combination");
            }

            if (terminator != null && !StringUtils.isBlank(terminator) &&
                    termination == null) {
                throw new AssertionError("Cannot proceed with terminated account. Invalid terminator and termination combination");
            }
        }
    }
}