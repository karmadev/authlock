/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.server;

import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.server.network.NetworkMessage;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.server.network.NNetworkMessage;
import org.jetbrains.annotations.NotNull;

import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class SServer implements Server {

    private final Collection<OfflineClient> clients = ConcurrentHashMap.newKeySet();

    private final ServerBridge bridge;
    private final UnsignedBigInteger id;
    private final String name;
    private final InetAddress address;
    private final int port;

    public SServer(final ServerBridge bridge, final UnsignedBigInteger id, final String name,
                   final InetAddress address, final int port) {
        this.bridge = bridge;
        this.id = id;
        this.name = name;
        this.address = address;
        this.port = port;
    }

    /**
     * Get the SQL entity id
     *
     * @return the entity id
     */
    @Override
    public @NotNull UnsignedBigInteger getEntityId() {
        return this.id;
    }

    /**
     * Get the server name
     *
     * @return the server name
     */
    @Override
    public String getName() {
        return this.name;
    }

    /**
     * Get the server address
     *
     * @return the server address
     */
    @Override
    public InetAddress getAddress() {
        return this.address;
    }

    /**
     * Get the server port
     *
     * @return the server port
     */
    @Override
    public int getPort() {
        return this.port;
    }

    /**
     * Get if the server is currently
     * connected
     *
     * @return if the server is currently
     * connected to the network. This value
     * is directly provided by a constant ping
     * from the network to the server every
     * ten seconds.
     */
    @Override
    public boolean isConnected() {
        return this.bridge.isConnected(this.address, this.port);
    }

    /**
     * Get all the offline clients in the
     * server.
     * The offline clients may also
     * contain clients which are currently
     * online.
     * This list also represents the
     * clients who disconnected in this server
     *
     * @return the clients in the server
     */
    @Override
    public Collection<OfflineClient> getOfflineClients() {
        return Collections.unmodifiableCollection(this.clients);
    }

    public void addOfflineClient(final OfflineClient client) {
        this.clients.add(client);
    }

    public void removeOfflineClient(final OfflineClient client) {
        this.clients.remove(client);
    }

    /**
     * Send a message to the server. Is highly recommended
     * to use {@link #sendMessage(NetworkMessage, long)} or {@link Server#sendMessage(NetworkMessage, long, TimeUnit)}
     * as those will time out the message task. This method
     * will keep the task waiting until a response or an exception
     * is provided.
     *
     * @param message the message to send
     * @return the message task
     */
    @Override
    public CompletableFuture<Boolean> sendMessage(final NetworkMessage message) {
        NNetworkMessage networkMessage = NNetworkMessage.fromNetworkMessage(message);
        return this.bridge.sendMessage(this.address, this.port, networkMessage);
    }

    /**
     * Returns a hash code value for the object. This method is
     * supported for the benefit of hash tables such as those provided by
     * {@link HashMap}.
     * <p>
     * The general contract of {@code hashCode} is:
     * <ul>
     * <li>Whenever it is invoked on the same object more than once during
     *     an execution of a Java application, the {@code hashCode} method
     *     must consistently return the same integer, provided no information
     *     used in {@code equals} comparisons on the object is modified.
     *     This integer need not remain consistent from one execution of an
     *     application to another execution of the same application.
     * <li>If two objects are equal according to the {@link
     *     #equals(Object) equals} method, then calling the {@code
     *     hashCode} method on each of the two objects must produce the
     *     same integer result.
     * <li>It is <em>not</em> required that if two objects are unequal
     *     according to the {@link #equals(Object) equals} method, then
     *     calling the {@code hashCode} method on each of the two objects
     *     must produce distinct integer results.  However, the programmer
     *     should be aware that producing distinct integer results for
     *     unequal objects may improve the performance of hash tables.
     * </ul>
     *
     * @return a hash code value for this object.
     * @implSpec As far as is reasonably practical, the {@code hashCode} method defined
     * by class {@code Object} returns distinct integers for distinct objects.
     * @see Object#equals(Object)
     * @see System#identityHashCode
     */
    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    /**
     * Indicates whether some other object is "equal to" this one.
     * <p>
     * The {@code equals} method implements an equivalence relation
     * on non-null object references:
     * <ul>
     * <li>It is <i>reflexive</i>: for any non-null reference value
     *     {@code x}, {@code x.equals(x)} should return
     *     {@code true}.
     * <li>It is <i>symmetric</i>: for any non-null reference values
     *     {@code x} and {@code y}, {@code x.equals(y)}
     *     should return {@code true} if and only if
     *     {@code y.equals(x)} returns {@code true}.
     * <li>It is <i>transitive</i>: for any non-null reference values
     *     {@code x}, {@code y}, and {@code z}, if
     *     {@code x.equals(y)} returns {@code true} and
     *     {@code y.equals(z)} returns {@code true}, then
     *     {@code x.equals(z)} should return {@code true}.
     * <li>It is <i>consistent</i>: for any non-null reference values
     *     {@code x} and {@code y}, multiple invocations of
     *     {@code x.equals(y)} consistently return {@code true}
     *     or consistently return {@code false}, provided no
     *     information used in {@code equals} comparisons on the
     *     objects is modified.
     * <li>For any non-null reference value {@code x},
     *     {@code x.equals(null)} should return {@code false}.
     * </ul>
     *
     * <p>
     * An equivalence relation partitions the elements it operates on
     * into <i>equivalence classes</i>; all the members of an
     * equivalence class are equal to each other. Members of an
     * equivalence class are substitutable for each other, at least
     * for some purposes.
     *
     * @param obj the reference object with which to compare.
     * @return {@code true} if this object is the same as the obj
     * argument; {@code false} otherwise.
     * @implSpec The {@code equals} method for class {@code Object} implements
     * the most discriminating possible equivalence relation on objects;
     * that is, for any non-null reference values {@code x} and
     * {@code y}, this method returns {@code true} if and only
     * if {@code x} and {@code y} refer to the same object
     * ({@code x == y} has the value {@code true}).
     * <p>
     * In other words, under the reference equality equivalence
     * relation, each equivalence class only has a single element.
     * @apiNote It is generally necessary to override the {@link #hashCode() hashCode}
     * method whenever this method is overridden, so as to maintain the
     * general contract for the {@code hashCode} method, which states
     * that equal objects must have equal hash codes.
     * @see #hashCode()
     * @see HashMap
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == this)
            return true;

        if (!(obj instanceof Server)) return false;
        Server other = (Server) obj;

        return Objects.equals(this.id, other.getEntityId());
    }

    /**
     * Returns a string representation of the object.
     *
     * @return a string representation of the object.
     * @apiNote In general, the
     * {@code toString} method returns a string that
     * "textually represents" this object. The result should
     * be a concise but informative representation that is easy for a
     * person to read.
     * It is recommended that all subclasses override this method.
     * The string output is not necessarily stable over time or across
     * JVM invocations.
     * @implSpec The {@code toString} method for class {@code Object}
     * returns a string consisting of the name of the class of which the
     * object is an instance, the at-sign character `{@code @}', and
     * the unsigned hexadecimal representation of the hash code of the
     * object. In other words, this method returns a string equal to the
     * value of:
     * <blockquote>
     * <pre>
     * getClass().getName() + '@' + Integer.toHexString(hashCode())
     * </pre></blockquote>
     */
    @Override
    public String toString() {
        return String.format("Server{name: %s, address: %s:%d}",
                this.name,
                this.address,
                this.port);
    }
}