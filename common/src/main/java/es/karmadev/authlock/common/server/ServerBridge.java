/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.server;

import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.common.server.network.NNetworkMessage;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;

@SuppressWarnings("unused")
public class ServerBridge {

    private final Set<InetSocketAddress> connectedServers = ConcurrentHashMap.newKeySet();
    private final BiFunction<InetSocketAddress, NNetworkMessage, CompletableFuture<Boolean>> onMessage;

    public ServerBridge(final BiFunction<InetSocketAddress, NNetworkMessage, CompletableFuture<Boolean>> onMessage) {
        Preconditions.assertNotNull(onMessage, "onMessage");
        this.onMessage = onMessage;
    }

    public void setConnected(final InetAddress address, final int port) {
        this.connectedServers.add(new InetSocketAddress(address, port));
    }

    public void setDisconnected(final InetAddress address, final int port) {
        this.connectedServers.remove(new InetSocketAddress(address, port));
    }

    public boolean isConnected(final InetAddress address, final int port) {
        return this.connectedServers.contains(new InetSocketAddress(address, port));
    }

    public CompletableFuture<Boolean> sendMessage(final InetAddress address, final int port, final NNetworkMessage message) {
        return this.onMessage.apply(
                new InetSocketAddress(address, port),
                message
        );
    }
}
