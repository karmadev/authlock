/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client.instance.online;

import es.karmadev.authlock.api.util.Preconditions;

import java.util.function.Consumer;
import java.util.function.Supplier;

/**
 * A bridge which allows to perform operations
 * from bukkit, bungee and bedrock directly into
 * the online client. For instance, sending a
 * message
 */
public final class OnlineClientBridge {

    private final Consumer<String> onMessage;
    private final Consumer<String> onKick;
    private final Supplier<Boolean> isConnected;

    public OnlineClientBridge(final Consumer<String> onMessage, final Consumer<String> onKick, final Supplier<Boolean> isConnected) {
        Preconditions.assertNotNull(onMessage, "onMessage");
        Preconditions.assertNotNull(onKick, "onKick");
        Preconditions.assertNotNull(isConnected, "isConnected");

        this.onMessage = onMessage;
        this.onKick = onKick;
        this.isConnected = isConnected;
    }

    public void sendMessage(final String message) {
        this.onMessage.accept(message);
    }

    public void kick(final String reason) {
        this.onKick.accept(reason);
    }

    public boolean isConnected() {
        return isConnected.get();
    }
}