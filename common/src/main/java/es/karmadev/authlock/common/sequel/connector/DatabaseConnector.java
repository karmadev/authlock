/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.sequel.connector;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.plugin.file.database.OptsConnection;
import es.karmadev.authlock.api.plugin.file.database.OptsDatabase;
import es.karmadev.authlock.api.plugin.file.database.OptsExecutor;
import es.karmadev.authlock.api.plugin.file.database.OptsPool;
import es.karmadev.authlock.api.sequel.model.column.SQColumn;
import es.karmadev.authlock.api.sequel.model.table.SQTable;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.sequel.connector.schema.RenameData;
import es.karmadev.authlock.common.sequel.connector.schema.SchemaStorage;
import es.karmadev.authlock.common.sequel.worker.DynamicThreadPool;
import es.karmadev.authlock.common.sequel.worker.ExecutorWrapper;
import org.jooq.Constraint;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.Table;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;

import java.nio.file.Path;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;

import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.*;

/**
 * Database connector
 */
@SuppressWarnings("unused")
public final class DatabaseConnector {

    private final SQLDialect driver;

    private HikariDataSource dataSource;
    private DSLContext connectionContext;

    private final DynamicThreadPool pool = new DynamicThreadPool(2, 10, 60);

    /**
     * Create the database connector
     *
     * @param driver the driver
     */
    public DatabaseConnector(final SQLDialect driver) {
        this.driver = driver;
    }

    /**
     * Configure the connector. This
     * configures the {@link #pool} and
     * tries to connect to the
     * database if not yet
     */
    public CompletableFuture<Void> configure() {
        AuthLock plugin = AuthLock.getInstance();
        if (plugin == null)
            throw new InitializationRequiredException(DatabaseConnector.class, "configure");

        if (!this.isConnected()) {
            this.connect();
            return this.setupDatabase()
                    .thenAccept(ignored -> configurePool())
                    .exceptionally(exception -> {
                        configurePool();

                        final String message = "Failed to configure database";
                        if (exception != null) {
                            plugin.getLogger().error(exception, message);
                        } else {
                            plugin.getLogger().severe(message);
                        }

                        return null;
                    });
        }

        return CompletableFuture.supplyAsync(() -> {
            this.configurePool();
            return null;
        });
    }

    /**
     * Configure the pool
     */
    private void configurePool() {
        AuthLock plugin = AuthLock.getInstance();
        assert plugin != null;

        OptsDatabase opts = plugin.getDatabaseOpts();
        OptsExecutor execution = opts.getExecutor();

        int min = Math.min(execution.getMinThreads(), execution.getMaxThreads());
        int max = Math.max(execution.getMinThreads(), execution.getMaxThreads());
        long keepAlive = Math.max(10, Math.min(60, execution.getThreadLifeTime()));

        this.pool.setCorePoolSize(min);
        this.pool.setMaximumPoolSize(max);
        this.pool.setKeepAliveTime(keepAlive, TimeUnit.SECONDS);
    }

    /**
     * Get if the database connector
     * is connected
     *
     * @return the connection status
     */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isConnected() {
        return this.dataSource != null &&
                !this.dataSource.isClosed();
    }

    /**
     * Connect to the database
     * @throws InitializationRequiredException if the authlock plugin has not
     * been yet initialized
     */
    private void connect() throws InitializationRequiredException {
        AuthLock plugin = AuthLock.getInstance();
        if (plugin == null)
            throw new InitializationRequiredException(DatabaseConnector.class, "connect");

        OptsDatabase opts = plugin.getDatabaseOpts();
        OptsConnection connection = opts.getConnection();

        String host = connection.getHost();
        int port = connection.getPort();
        String database = connection.getDatabase();
        String user = connection.getUser();
        String password = connection.getPassword();

        OptsPool poolSettings = opts.getPooling();
        if (this.dataSource == null || this.dataSource.isClosed()) {
            HikariConfig config = new HikariConfig();
            config.setJdbcUrl(buildJDBCUrl(plugin, this.driver, host, port, database));
            config.setUsername(user);
            config.setPassword(password);

            config.setMinimumIdle(poolSettings.getMinConnections());
            config.setMaximumPoolSize(poolSettings.getMaxConnections());
            config.setIdleTimeout(poolSettings.getIdleTimeout() * 1000L);
            config.setConnectionTimeout(poolSettings.getConnectionTimeout() * 1000L);

            config.setConnectionTestQuery("SELECT 1");

            config.addDataSourceProperty("cachePrepStmts", "true");
            config.addDataSourceProperty("prepStmtCacheSize", "250");
            config.addDataSourceProperty("prepStmtCacheSqlLimit", "2048");

            this.dataSource = new HikariDataSource(config);
        }
    }

    private String buildJDBCUrl(final AuthLock plugin, final SQLDialect driver, final String host, final int port, final String dbName) {
        String database = getDatabaseAbsoluteName(dbName);
        if (driver.equals(SQLDialect.SQLITE)) {
            Path workingDirectory = plugin.getDataFolder().toAbsolutePath();
            if (!database.endsWith(".db"))
                database = String.format("%s.db", database);

            Path dbFile = workingDirectory.resolve(database);
            return String.format("jdbc:%s:%s", driver.getNameLC(), dbFile);
        }

        return String.format("jdbc:%s://%s:%d/%s", driver.thirdParty().driver(), host, port, database);
    }

    private String getDatabaseAbsoluteName(final String dbName) {
        String database = dbName.replace("\\", "/");

        if (database.contains("/")) {
            String[] dbNameData = database.split("/");
            int index = 1;
            while (index != dbNameData.length) {
                String part = dbNameData[dbNameData.length - (index++)];
                if (part == null || part.trim().isEmpty())
                    continue;

                return part;
            }
        }

        return database.replace("/", "");
    }

    /**
     * Prepare the database in the existing connection
     * with the required plugin tables
     *
     * @throws InitializationRequiredException if the plugin has not been yet
     * initialized
     */
    private CompletableFuture<Void> setupDatabase() throws InitializationRequiredException {
        return CompletableFuture.supplyAsync(() -> {
            AuthLock plugin = AuthLock.getInstance();
            if (plugin == null)
                throw new InitializationRequiredException(DatabaseConnector.class, "setupDatabase");

            if (!this.isConnected())
                this.connect();

            DSLContext context = this.getContext();
            SchemaStorage storage = new SchemaStorage(plugin);

            try {
                createServerTable(storage);
                createSessionTable(storage);
                createAccountTable(storage);
                createUUIDTable(storage);
                createUserTable(storage);
            } catch (SQLException ex) {
                throw new SQLDaoException(ex);
            }

            return null;
        });
    }

    /**
     * Close the connector connection
     */
    public void close() {
        if (!this.isConnected())
            return;

        this.dataSource.close();
        this.dataSource = null;
        this.connectionContext = null;
    }

    /**
     * Get the DSL context
     *
     * @return the connector DSL context
     * @throws IllegalStateException if the connection has not been
     * yet performed
     */
    public DSLContext getContext() throws IllegalStateException {
        if (!this.isConnected())
            throw new IllegalStateException("Cannot create DLS context without a valid SQL connection");

        if (this.connectionContext == null)
            this.connectionContext = DSL.using(this.dataSource, this.driver);

        return this.connectionContext;
    }

    /**
     * Retrieve a DSL context with
     * an auto-commit set on false
     *
     * @return the connector DSL context
     * @throws IllegalStateException if the connection has not been
     * yet performed
     * @throws SQLException if there's a problem retrieving the
     * connection
     */
    public DSLContext createTransaction() throws IllegalStateException, SQLException {
        if (!this.isConnected())
            throw new IllegalStateException("Cannot create DLS context without a valid SQL connection");

        Connection connection = this.dataSource.getConnection();
        connection.setAutoCommit(false);

        return DSL.using(connection, this.driver);
    }

    /**
     * Get the pool executor
     *
     * @return the executor
     */
    public Executor getExecutor() {
        return ExecutorWrapper.wrap(pool::execute);
    }

    private void createUserTable(final SchemaStorage schema) throws SQLException {
        Collection<RenameData> schemaUpdates = schema.detectUserSchema();
        if (!schemaUpdates.isEmpty()) {
            updateExistingTableSchema(user(), schemaUpdates, schema::updateUserSchema);
            return;
        }

        DSLContext context = this.getContext();
        if (tableDoesExists(user(), context))
            return;

        context.createTableIfNotExists(user().getTableName())
                .column(user().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED.identity(true))
                .column(user().getColumnName(USERNAME), SQLDataType.VARCHAR(16).notNull())
                .column(user().getColumnName(UUID), SQLDataType.BIGINTUNSIGNED.notNull())
                .column(user().getColumnName(ACCOUNT), SQLDataType.BIGINTUNSIGNED.notNull())
                .column(user().getColumnName(SESSION), SQLDataType.BIGINTUNSIGNED.notNull())
                .column(user().getColumnName(SERVER), SQLDataType.BIGINTUNSIGNED.notNull())
                .column(user().getColumnName(ADDRESS), SQLDataType.VARCHAR(39).nullable(true).defaultValue((String) null))
                .column(user().getColumnName(CREATION), SQLDataType.TIMESTAMP.notNull().defaultValue(DSL.currentTimestamp()))
                .constraints(
                        primaryKey("user_id"),
                        foreignKey("user_uuid", user().getColumnName(UUID), uuid().getTableName()),
                        foreignKey("user_account", user().getColumnName(ACCOUNT), account().getTableName()),
                        foreignKey("user_session", user().getColumnName(SESSION), session().getTableName()),
                        foreignKey("user_server", user().getColumnName(SERVER), server().getTableName())
                ).execute();
    }

    private void createServerTable(final SchemaStorage schema) throws SQLException {
        Collection<RenameData> schemaUpdates = schema.detectServerSchema();
        if (!schemaUpdates.isEmpty()) {
            updateExistingTableSchema(server(), schemaUpdates, schema::updateServerSchema);
            return;
        }

        DSLContext context = this.getContext();
        if (tableDoesExists(server(), context))
            return;

        context.createTableIfNotExists(server().getTableName())
                .column(server().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED.identity(true))
                .column(server().getColumnName(NAME), SQLDataType.VARCHAR(64).notNull())
                .column(server().getColumnName(ADDRESS), SQLDataType.VARCHAR(39).notNull())
                .column(server().getColumnName(PORT), SQLDataType.SMALLINTUNSIGNED.notNull())
                .constraints(
                        primaryKey("server_id")
                ).execute();
    }

    private void createSessionTable(final SchemaStorage schema) throws SQLException {
        Collection<RenameData> schemaUpdates = schema.detectSessionSchema();
        if (!schemaUpdates.isEmpty()) {
            updateExistingTableSchema(session(), schemaUpdates, schema::updateSessionSchema);
            return;
        }

        DSLContext context = this.getContext();
        if (tableDoesExists(session(), context))
            return;

        context.createTableIfNotExists(session().getTableName())
                .column(session().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED.identity(true))
                .column(session().getColumnName(PASSWORD), SQLDataType.BOOLEAN.notNull().defaultValue(false))
                .column(session().getColumnName(PIN), SQLDataType.BOOLEAN.notNull().defaultValue(false))
                .column(session().getColumnName(TOTP), SQLDataType.BOOLEAN.notNull().defaultValue(false))
                .column(session().getColumnName(VALIDATION), SQLDataType.TIMESTAMP.notNull().defaultValue(DSL.currentTimestamp()))
                .constraints(
                        primaryKey("session_id")
                ).execute();
    }

    private void createAccountTable(final SchemaStorage schema) throws SQLException {
        Collection<RenameData> schemaUpdates = schema.detectAccountSchema();
        if (!schemaUpdates.isEmpty()) {
            updateExistingTableSchema(account(), schemaUpdates, schema::updateAccountSchema);
            return;
        }

        DSLContext context = this.getContext();
        if (tableDoesExists(account(), context))
            return;

        context.createTableIfNotExists(account().getTableName())
                .column(account().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED.identity(true))
                .column(account().getColumnName(PASSWORD), SQLDataType.LONGVARCHAR.nullable(true).defaultValue((String) null))
                .column(account().getColumnName(PIN), SQLDataType.LONGVARCHAR.nullable(true).defaultValue((String) null))
                .column(account().getColumnName(TOTP), SQLDataType.LONGVARCHAR.nullable(true).defaultValue((String) null))
                .column(account().getColumnName(TERMINATOR), SQLDataType.VARCHAR(16).nullable(true).defaultValue((String) null))
                .column(account().getColumnName(TERMINATION), SQLDataType.TIMESTAMP.nullable(true).defaultValue((Timestamp) null))
                .column(account().getColumnName(CREATION), SQLDataType.TIMESTAMP.notNull().defaultValue(DSL.currentTimestamp()))
                .constraints(
                        primaryKey("account_id")
                ).execute();
    }

    private void createUUIDTable(final SchemaStorage schema) throws SQLException {
        Collection<RenameData> schemaUpdates = schema.detectUUIDSchema();
        if (!schemaUpdates.isEmpty()) {
            updateExistingTableSchema(uuid(), schemaUpdates, schema::updateUUIDSchema);
            return;
        }

        DSLContext context = this.getContext();
        if (tableDoesExists(uuid(), context))
            return;

        context.createTableIfNotExists(uuid().getTableName())
                .column(uuid().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED.identity(true))
                .column(uuid().getColumnName(OFFLINE), SQLDataType.VARCHAR(36).notNull())
                .column(uuid().getColumnName(ONLINE), SQLDataType.VARCHAR(36).nullable(true).defaultValue((String) null))
                .column(uuid().getColumnName(BEDROCK), SQLDataType.VARCHAR(36).nullable(true).defaultValue((String) null))
                .column(uuid().getColumnName(ACTIVE), SQLDataType.CHAR(7).notNull().defaultValue("offline"))
                .constraints(
                        primaryKey("uuid_id")
                ).execute();
    }

    private static boolean tableDoesExists(final SQTable table, final DSLContext context) {
        List<Table<?>> tables = context.meta().getTables();
        if (tables.stream().anyMatch(t -> t.getName().equals(
                table.getTableName()
        ))) {
            AuthLock plugin = AuthLock.getInstance();
            assert plugin != null;

            plugin.getLogger().info("Table {} successfully validated",
                    table.getName());
            return true;
        }

        return false;
    }

    private void updateExistingTableSchema(final SQTable table, final Collection<RenameData> schemaUpdates,
                                           Runnable onEnd) throws SQLException {
        AuthLock plugin = AuthLock.getInstance();
        assert plugin != null;

        plugin.getLogger().warning("Detected changes in table schema of {}. Preparing to perform changes",
                table.getName());

        DSLContext transaction = this.createTransaction();
        transaction.transaction(settings -> {
            DSLContext context = DSL.using(settings);

            String tableName = table.getTableName();
            RenameData tableRenameData = schemaUpdates.stream()
                    .filter(RenameData::isTable)
                    .filter(u -> u.getExpected().equals(tableName))
                    .findAny().orElse(null);

            String tableToUpdate = tableName;

            if (tableRenameData != null)
                tableToUpdate = tableRenameData.getCurrent();

            for (SQColumn column : table.getColumns()) {
                String columnName = table.getColumnName(column);
                RenameData columnRenameData = schemaUpdates.stream()
                        .filter(u -> !u.isTable())
                        .filter(u -> u.getExpected().equals(columnName))
                        .findAny().orElse(null);

                if (columnRenameData == null)
                    continue;

                context.alterTable(tableToUpdate)
                        .renameColumn(columnRenameData.getCurrent())
                        .to(columnRenameData.getExpected()).execute();

                plugin.getLogger().warning("Renamed column from {} to {} in table {}",
                        columnRenameData.getCurrent(), columnRenameData.getExpected(), table.getName());
            }

            if (tableRenameData != null) {
                context.alterTable(tableRenameData.getCurrent())
                        .renameTo(tableRenameData.getExpected())
                        .execute();

                plugin.getLogger().warning("Renamed table {} to {}",
                        tableRenameData.getCurrent(),
                        tableRenameData.getExpected());
            }

            onEnd.run();
        });
    }

    private Constraint primaryKey(final String pkName) {
        return DSL.constraint(pkName).primaryKey("id");
    }

    private Constraint foreignKey(final String fgnName, final String fgnTarget, final String fgnTable) {
        return DSL.constraint(fgnName)
                .foreignKey(fgnTarget)
                .references(fgnTable, "id")
                .onDeleteRestrict()
                .onUpdateCascade();
    }
}
