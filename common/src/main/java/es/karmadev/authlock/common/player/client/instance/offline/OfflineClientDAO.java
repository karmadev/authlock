/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.client.instance.offline;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.types.ULong;

import java.util.concurrent.CompletableFuture;

import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.user;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.uuid;

final class OfflineClientDAO {

    private final DatabaseConnector connector;

    public OfflineClientDAO(final DatabaseConnector connector) {
        this.connector = connector;
    }

    public CompletableFuture<Void> updateName(final UnsignedBigInteger userId, final String name) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                context.update(DSL.table(user().getTableName()))
                        .set(DSL.field(user().getColumnName(USERNAME)), name)
                        .where(DSL.field(user().getColumnName(IDENTIFIER)).equal(ULong.valueOf(userId)))
                        .execute();

                return null;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    public CompletableFuture<Void> updateUUID(final UnsignedBigInteger userId, final UUIDStore store) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                context.update(DSL.table(uuid().getTableName()))
                        .set(DSL.field(uuid().getTableName(), uuid().getColumnName(OFFLINE)), store.getOffline())
                        .set(DSL.field(uuid().getTableName(), uuid().getColumnName(ONLINE)), store.getPremium())
                        .set(DSL.field(uuid().getTableName(), uuid().getColumnName(BEDROCK)), store.getBedrock())
                        .from(DSL.table(user().getTableName()))
                        .where(DSL.field(user().getTableName(), user().getColumnName(IDENTIFIER))
                                .eq(ULong.valueOf(userId)))
                        .and(DSL.field(uuid().getTableName(), uuid().getColumnName(IDENTIFIER))
                                .eq(DSL.field(user().getTableName(), user().getColumnName(IDENTIFIER))))
                        .execute();

                return null;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    public CompletableFuture<Void> updateServer(final UnsignedBigInteger userId, final UnsignedBigInteger serverId) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                UpdateConditionStep<Record> updateRecord = context.update(DSL.table(user().getTableName()))
                        .set(DSL.field(user().getColumnName(SERVER)), ULong.valueOf(serverId))
                        .where(DSL.field(user().getColumnName(IDENTIFIER)).equal(ULong.valueOf(userId)));

                updateRecord.execute();
                return null;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        });
    }
}
