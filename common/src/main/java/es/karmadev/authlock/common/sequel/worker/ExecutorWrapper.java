/*
Copyright (C) 2024 KarmaDev

This program is free software: you can redistribute it and/or modify it under the terms of
the GNU General Public License as published by the Free Software Foundation, version
3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this
program. If not, see https://www.gnu.org/licenses/.
*/

package es.karmadev.authlock.common.sequel.worker;

import org.jetbrains.annotations.NotNull;

import java.util.concurrent.Executor;
import java.util.function.Consumer;

public final class ExecutorWrapper implements Executor {

    private final Consumer<Runnable> commandExecutor;

    private ExecutorWrapper(final Consumer<Runnable> commandExecutor) {
        this.commandExecutor = commandExecutor;
    }

    /**
     * Executes the given command at some time in the future.  The command
     * may execute in a new thread, in a pooled thread, or in the calling
     * thread, at the discretion of the {@code Executor} implementation.
     *
     * @param command the runnable task
     * @throws java.util.concurrent.RejectedExecutionException if this task cannot be
     *                                    accepted for execution
     * @throws NullPointerException       if command is null
     */
    @Override
    public void execute(final @NotNull Runnable command) {
        this.commandExecutor.accept(command);
    }

    /**
     * Create a new executor
     * wrapper
     *
     * @param onCommand the command consumer
     * @return the executor wrapper
     */
    public static ExecutorWrapper wrap(final Consumer<Runnable> onCommand) {
        return new ExecutorWrapper(onCommand);
    }
}
