/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.session;

import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jooq.DSLContext;
import org.jooq.Record1;
import org.jooq.UpdateResultStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.types.ULong;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.session;

final class SessionDAO {

    private final DatabaseConnector connector;

    public SessionDAO(final DatabaseConnector connector) {
        this.connector = connector;
    }

    public CompletableFuture<Instant> updatePassword(final UnsignedBigInteger sessionId, final boolean newPassword) {
        return this.updateEntry(sessionId, session().getColumnName(PASSWORD), newPassword)
                .thenCompose(rs -> {
                    if (!newPassword)
                        return setValidity(sessionId);

                    return CompletableFuture.completedFuture(null);
                });
    }

    public CompletableFuture<Void> updatePin(final UnsignedBigInteger sessionId, final boolean newPin) {
        return this.updateEntry(sessionId, session().getColumnName(PIN), newPin);
    }

    public CompletableFuture<Void> updateTotp(final UnsignedBigInteger sessionId, final boolean newPassword) {
        return this.updateEntry(sessionId, session().getColumnName(TOTP), newPassword);
    }

    public CompletableFuture<Instant> setValidity(final UnsignedBigInteger sessionId) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                UpdateResultStep<Record1<Timestamp>> step = context.update(DSL.table(session().getTableName()))
                        .set(DSL.field(session().getColumnName(VALIDATION)), DSL.currentTimestamp())
                        .where(DSL.field(session().getColumnName(IDENTIFIER)).eq(ULong.valueOf(sessionId)))
                        .returningResult(DSL.field(session().getColumnName(VALIDATION), SQLDataType.TIMESTAMP));
                Record1<Timestamp> result = step.fetchSingle();

                Timestamp termination = result.get(DSL.field(session().getColumnName(VALIDATION), Timestamp.class));
                if (termination == null)
                    return null;

                return termination.toInstant();
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    private CompletableFuture<Void> updateEntry(final UnsignedBigInteger sessionId, final String col, final boolean newValue) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                context.update(DSL.table(session().getTableName()))
                        .set(DSL.field(col), newValue)
                        .where(DSL.field(session().getColumnName(IDENTIFIER)).equal(sessionId.longValue()))
                        .execute();

                return null;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }
}
