/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.player.account;

import es.karmadev.authlock.api.authority.AuthorityEntity;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.concurrent.CompletableFuture;

import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;
import static es.karmadev.authlock.api.sequel.model.table.SQTable.account;

final class AccountDAO {

    private final DatabaseConnector connector;

    public AccountDAO(final DatabaseConnector connector) {
        this.connector = connector;
    }

    public CompletableFuture<Boolean> updatePassword(final UnsignedBigInteger accountId, final String newPassword) {
        return this.updateEntry(accountId, "password", newPassword);
    }

    public CompletableFuture<Boolean> updatePin(final UnsignedBigInteger accountId, final String newPin) {
        return this.updateEntry(accountId, "pin", newPin);
    }

    public CompletableFuture<Boolean> updateTotp(final UnsignedBigInteger accountId, final String newPassword) {
        return this.updateEntry(accountId, "totp", newPassword);
    }

    public CompletableFuture<Instant> terminate(final UnsignedBigInteger accountId, final AuthorityEntity entity) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                Record result = context.update(DSL.table(account().getTableName()))
                        .set(DSL.field(account().getColumnName(TERMINATOR)), entity.getAuthorityName())
                        .set(DSL.field(account().getColumnName(TERMINATION)), DSL.currentTimestamp())
                        .where(DSL.field(account().getColumnName(IDENTIFIER)).equal(accountId))
                        .returning(DSL.field(account().getColumnName(TERMINATION)))
                        .fetchOne();

                if (result == null)
                    return null;

                Timestamp termination = result.get(DSL.field(account().getColumnName(TERMINATION), Timestamp.class));
                return termination.toInstant();
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    private CompletableFuture<Boolean> updateEntry(final UnsignedBigInteger accountId, final String col, final String newValue) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                context.update(DSL.table(account().getTableName())).set(DSL.field(col), newValue)
                        .where(DSL.field(account().getColumnName(IDENTIFIER)).equal(accountId.longValue())).execute();

                return true;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }
}
