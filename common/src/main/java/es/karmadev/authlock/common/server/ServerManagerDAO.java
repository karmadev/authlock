/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.common.server;

import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.common.exception.SQLDaoException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import org.jooq.*;
import org.jooq.Record;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.jooq.impl.SQLDataType;
import org.jooq.types.ULong;
import org.jooq.types.UShort;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.CompletableFuture;

import static es.karmadev.authlock.api.sequel.model.table.SQTable.*;
import static es.karmadev.authlock.api.sequel.model.column.SQColumn.*;

class ServerManagerDAO {

    private static final Field<ULong> ID_FIELD = field(server().getColumnName(IDENTIFIER), SQLDataType.BIGINTUNSIGNED);
    private static final Field<String> NAME_FIELD = field(server().getColumnName(NAME), SQLDataType.VARCHAR);
    private static final Field<String> ADDRESS_FIELD = field(server().getColumnName(ADDRESS), SQLDataType.VARCHAR);
    private static final Field<UShort> PORT_FIELD = field(server().getColumnName(PORT), SQLDataType.SMALLINTUNSIGNED);

    private final ServerBridge bridge;
    private final DatabaseConnector connector;

    public ServerManagerDAO(final ServerBridge bridge, final DatabaseConnector connector) {
        this.bridge = bridge;
        this.connector = connector;
    }

    public CompletableFuture<Server> updateServer(final UnsignedBigInteger serverId, final String newName, final InetAddress newAddress,
                                                  final int newPort) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                context.update(DSL.table(server().getTableName()))
                        .set(NAME_FIELD, newName)
                        .set(ADDRESS_FIELD, newAddress.getHostAddress())
                        .set(PORT_FIELD, UShort.valueOf(newPort))
                        .where(ID_FIELD.eq(ULong.valueOf(serverId)))
                        .execute();

                return new SServer(this.bridge, serverId, newName, newAddress, newPort);
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    public CompletableFuture<Server> insertServer(final String name, final InetAddress address, final int port) {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                Record result = context
                        .insertInto(DSL.table(server().getTableName()))
                        .columns(NAME_FIELD, ADDRESS_FIELD, PORT_FIELD)
                        .values(name, address.getHostAddress(), UShort.valueOf(port))
                        .returningResult(
                                ID_FIELD
                        )
                        .fetchSingle();

                ULong serverId = result.get(ID_FIELD);
                return new SServer(this.bridge, new UnsignedBigInteger(serverId), name, address, port);
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    public CompletableFuture<Collection<Server>> getAllServers() {
        return CompletableFuture.supplyAsync(() -> {
            DSLContext context = this.connector.getContext();
            try {
                List<Record4<ULong, String, String, UShort>> result = context
                        .select(
                                ID_FIELD,
                                NAME_FIELD,
                                ADDRESS_FIELD,
                                PORT_FIELD
                        )
                        .from(server().getTableName())
                        .fetch();

                Set<Server> servers = new HashSet<>();
                for (Record4<ULong, String, String, UShort> resultRecord : result) {
                    ULong serverId =  resultRecord.get(ID_FIELD);
                    String serverName = resultRecord.get(NAME_FIELD);
                    String rawAddress = resultRecord.get(ADDRESS_FIELD);
                    UShort port = resultRecord.get(PORT_FIELD);

                    if (serverId == null || serverName == null || rawAddress == null || port == null)
                        continue;

                    SServer server = new SServer(
                            this.bridge,
                            new UnsignedBigInteger(serverId),
                            serverName,
                            getOptimisticallyAddress(rawAddress),
                            port.intValue()
                    );

                    servers.add(server);
                }

                return servers;
            } catch (DataAccessException ex) {
                throw new SQLDaoException(ex);
            }
        }, this.connector.getExecutor());
    }

    private static <T> Field<T> field(final String name, final DataType<T> type) {
        return DSL.field(name, type);
    }

    private static InetAddress getOptimisticallyAddress(final String address) {
        try {
            return InetAddress.getByName(address);
        } catch (UnknownHostException ignored) {
            //We optimistically think that the address is always valid
        }
        return null;
    }
}
