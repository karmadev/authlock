/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.test.mock;

import es.karmadev.api.logger.Logger;
import es.karmadev.api.logger.LoggerManager;
import es.karmadev.authlock.api.player.client.ClientManager;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.plugin.Manager;
import es.karmadev.authlock.api.plugin.file.database.OptsDatabase;
import es.karmadev.authlock.api.server.ServerManager;
import es.karmadev.authlock.api.util.exception.plugin.NoSuchManagerException;
import es.karmadev.authlock.common.player.client.PlayerClientManager;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServerManager;
import es.karmadev.authlock.test.mock.opts.database.MockDatabaseOptions;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jooq.SQLDialect;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AuthLockMock extends AuthLock {

    private DatabaseConnector connector;
    private final Map<Class<? extends Manager>, Manager> managers = new HashMap<>();

    public DatabaseConnector getConnector() {
        if (this.connector == null) {
            this.connector = new DatabaseConnector(SQLDialect.SQLITE);

            this.managers.put(ClientManager.class, new PlayerClientManager(this.connector));
            this.managers.put(ServerManager.class, new SServerManager(null, this.connector));
        }

        return this.connector;
    }

    /**
     * Get the plugin database settings
     *
     * @return the database settings
     */
    @Override
    public @NotNull OptsDatabase getDatabaseOpts() {
        return new MockDatabaseOptions();
    }

    /**
     * Get the plugin logger
     *
     * @return the plugin logger
     */
    @Override
    public @NotNull Logger getLogger() {
        return LoggerManager.getInstance().getProvider().getLogger(AuthLockMock.class);
    }

    /**
     * Get a manager
     *
     * @param type the manager type
     * @return the manager
     * @throws NoSuchManagerException if the manager type does
     * not exist
     */
    @Override
    @SuppressWarnings("unchecked")
    @Contract("null -> fail")
    public <T extends Manager> @NotNull T getManager(final Class<T> type) throws NoSuchManagerException {
        if (type == null)
            throw new NoSuchManagerException();

        for (Map.Entry<Class<? extends Manager>, Manager> managerEntry : this.managers.entrySet()) {
            if (type.isAssignableFrom(managerEntry.getKey()))
                return (T) managerEntry.getValue();
        }

        throw new NoSuchManagerException(type);
    }

    /**
     * Get the plugin data folder
     *
     * @return the plugin data
     * folder
     */
    @Override
    public Path getDataFolder() {
        Path path = Paths.get("test")
                .resolve("unit");

        if (!Files.exists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        }

        return path;
    }

    /**
     * Request to export an internal
     * plugin resource to the specified
     * destination. If the destination is
     * a file, the resource file name will
     * be used.
     * Fail cases:
     * <ul>
     *     <li>If the resource does not exists</li>
     *     <li>If the resource is an entire directory</li>
     *     <li>If the destination file already exists</li>
     * </ul>
     * This method looks only inside "public" resources, for instance,
     * calling this method using plugin.yml as resource parameter will
     * fail unless there's a plugin.yml inside the "public" resources
     * directory
     *
     * @param resource    the resource to export
     * @param destination the resource destination file
     * @return if the export was successful
     */
    @Override
    public boolean requestExport(final String resource, final Path destination) {
        if (Files.exists(destination)) return false;

        try(InputStream stream = this.getClass().getResourceAsStream(String.format("/public/%s", resource))) {
            if (stream == null)
                return false;

            Path parent = destination.toAbsolutePath().getParent();
            if (!Files.exists(parent))
                Files.createDirectories(parent);

            Files.copy(stream, destination, StandardCopyOption.REPLACE_EXISTING);
            return true;
        } catch (IOException ex) {
            this.getLogger().error(ex, "Failed to export");
        }

        return false;
    }

    public void teardown() throws Exception {
        if (this.connector != null && this.connector.isConnected())
            this.connector.close();

        Path dataFolder = this.getDataFolder();
        this.removeRecursive(dataFolder);
        this.unregister();
    }

    private void unregister() throws ReflectiveOperationException {
        Class<AuthLock> authLockClass = AuthLock.class;
        Field instance = authLockClass.getDeclaredField("instance");

        instance.setAccessible(true);
        instance.set(authLockClass, null);
    }

    private void removeRecursive(final Path path) throws IOException {
        if (Files.isDirectory(path)) {
            try (Stream<Path> files = Files.list(path)) {
                for (Path file : files.collect(Collectors.toList()))
                    this.removeRecursive(file);
            }

            Files.delete(path);
            return;
        }

        Files.delete(path);
    }

    public static AuthLockMock mock() {
        AuthLockMock mock = new AuthLockMock();
        AuthLock.registerInstance(mock);

        return mock;
    }
}
