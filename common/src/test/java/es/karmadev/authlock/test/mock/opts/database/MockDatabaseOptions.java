/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.test.mock.opts.database;

import es.karmadev.authlock.api.plugin.file.database.OptsConnection;
import es.karmadev.authlock.api.plugin.file.database.OptsDatabase;
import es.karmadev.authlock.api.plugin.file.database.OptsExecutor;
import es.karmadev.authlock.api.plugin.file.database.OptsPool;
import es.karmadev.authlock.api.sequel.DBDriver;
import es.karmadev.authlock.test.mock.opts.database.options.MockConnectionOptions;
import es.karmadev.authlock.test.mock.opts.database.options.MockExecutionOptions;
import es.karmadev.authlock.test.mock.opts.database.options.MockPoolOptions;

public final class MockDatabaseOptions implements OptsDatabase {

    /**
     * Get the current driver
     *
     * @return the driver
     */
    @Override
    public DBDriver getDriver() {
        return DBDriver.SQLITE;
    }

    /**
     * Get the connection settings
     *
     * @return the connection settings
     */
    @Override
    public OptsConnection getConnection() {
        return new MockConnectionOptions();
    }

    /**
     * Get the connection pooling
     * settings
     *
     * @return the pooling settings
     */
    @Override
    public OptsPool getPooling() {
        return new MockPoolOptions();
    }

    /**
     * Get the executor thread
     * settings
     *
     * @return the executor settings
     */
    @Override
    public OptsExecutor getExecutor() {
        return new MockExecutionOptions();
    }
}
