/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.test;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.player.client.ClientManager;
import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.server.ServerManager;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import es.karmadev.authlock.api.util.exception.plugin.NoSuchManagerException;
import es.karmadev.authlock.common.sequel.connector.DatabaseConnector;
import es.karmadev.authlock.common.server.SServer;
import es.karmadev.authlock.test.mock.AuthLockMock;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

final class MainTest {

    @SuppressWarnings({"FieldCanBeLocal", "unused"})
    private AuthLockMock mock;

    @BeforeEach
    public void init() {
        mock = AuthLockMock.mock();
    }

    @Test
    void testThatNotInitializedPluginFails() throws Exception {
        mock.teardown();

        DatabaseConnector connector = mock.getConnector();
        assertThrows(InitializationRequiredException.class, connector::configure);
    }

    @Test
    void testThatTheDatabaseConnectorCanConnectAndCreatesTables() {
        DatabaseConnector connector = mock.getConnector();
        assertNotNull(connector);

        assertDoesNotThrow(connector.configure()::join);
    }

    @Test
    void testThatNullManagerCannotBeRetrieved() {
        assertThrows(NoSuchManagerException.class, () -> mock.getManager(null));
    }

    @Test
    void testThatServersCanBeCreated() throws UnknownHostException {
        final String serverName = "test";
        final InetAddress address = InetAddress.getByName("127.0.0.1");
        final int port = 25565;

        SServer vServer = new SServer(null, new UnsignedBigInteger(1), serverName, address, port);

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ServerManager serverManager = assertDoesNotThrow(mock::getServerManager);
        Server server = serverManager.getServer(serverName);

        assertNull(server);

        Server createdServer = assertDoesNotThrow(serverManager.insertOrUpdateServer(serverName, address, port)::join);
        assertNotNull(createdServer);

        assertEquals(serverName, createdServer.getName());
        assertEquals(address, createdServer.getAddress());
        assertEquals(port, createdServer.getPort());

        assertEquals(vServer, createdServer);
    }

    @Test
    void testThatServersCanBeModified() throws UnknownHostException {
        final String serverName = "test";
        final InetAddress address = InetAddress.getByName("127.0.0.1");
        final int port = 25565;

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ServerManager serverManager = mock.getServerManager();
        Server createdServer = serverManager.insertOrUpdateServer(serverName, address, port).join();

        assertNotNull(createdServer);
        Server updatedServer = serverManager.insertOrUpdateServer("test2", address, port).join();

        assertEquals(createdServer, updatedServer);
    }

    @Test
    void testThatUsersCanSwitchServers() throws UnknownHostException {
        final String serverName1 = "test";
        final InetAddress address1 = InetAddress.getByName("127.0.0.1");
        final int port1 = 25565;

        final String serverName2 = "test2";
        final InetAddress address2 = InetAddress.getByName("127.0.0.1");
        final int port2 = 25566;

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ClientManager clientManager = assertDoesNotThrow(mock::getClientManager);
        clientManager.prepareCache().join();

        ServerManager serverManager = mock.getServerManager();
        Server createdServer1 = serverManager.insertOrUpdateServer(serverName1, address1, port1).join();
        Server createdServer2 = serverManager.insertOrUpdateServer(serverName2, address2, port2).join();

        OfflineClient client = clientManager.getOfflineClient("KarmaDev");
        assertNull(client);

        Boolean created = assertDoesNotThrow(() -> clientManager.createClient("KarmaDev", null, null, createdServer1)
                .join());
        assertTrue(created);

        client = clientManager.getOfflineClient("KarmaDev");
        assertNotNull(client);
        assertNotNull(client.getEntityId());

        assertTrue(createdServer1.isConnected(client));
        assertFalse(createdServer2.isConnected(client));

        Boolean result = assertDoesNotThrow(client.setServer(createdServer2)::join);
        assertTrue(result);

        assertFalse(createdServer1.isConnected(client));
        assertTrue(createdServer2.isConnected(client));
    }

    @Test
    void testThatUsersCanBeCreated() throws UnknownHostException {
        final String username = "KarmaDev";
        final UUIDStore store = UUIDStore.offlineId(
                UUID.nameUUIDFromBytes(("OfflinePlayer:KarmaDev").getBytes())
        );
        final String serverName = "test";
        final InetAddress address = InetAddress.getByName("127.0.0.1");
        final int port = 25565;

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ClientManager clientManager = assertDoesNotThrow(mock::getClientManager);
        assertDoesNotThrow(clientManager.prepareCache()::join);

        ServerManager serverManager = mock.getServerManager();
        Server createdServer = serverManager.insertOrUpdateServer(serverName, address, port).join();

        OfflineClient client = clientManager.getOfflineClient("KarmaDev");
        assertNull(client);

        Boolean created = assertDoesNotThrow(() -> clientManager.createClient("KarmaDev", null, null, createdServer)
                .join());
        assertTrue(created);

        client = clientManager.getOfflineClient("KarmaDev");
        assertNotNull(client);
        assertNotNull(client.getEntityId());

        assertNotNull(client.getUUID());

        assertEquals(username, client.getName());
        UUIDStore clientStore = client.getUUID();

        assertTrue(clientStore.hasOffline());
        assertFalse(clientStore.hasPremium());
        assertFalse(clientStore.hasBedrock());

        assertEquals(store.getOffline(), clientStore.getOffline());
        assertEquals(store.getPremium(), clientStore.getPremium());
        assertEquals(store.getBedrock(), clientStore.getBedrock());

        assertTrue(clientStore.contains(store.getOffline()));
        assertTrue(clientStore.containsAny(store));
        assertEquals(store, clientStore);

        assertNotNull(client.getAccount());
        assertNotNull(client.getSession());
        assertNotNull(client.getCreation());
    }

    @Test
    void testThatUserAccountCanBeModified() throws UnknownHostException {
        final String EMPTY_STR = "";
        final String newPassword = "HelloWorld";
        final String newPin = "0000";
        final String newTotp = "Base64String";
        final String serverName = "test";
        final InetAddress address = InetAddress.getByName("127.0.0.1");
        final int port = 25565;

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ServerManager serverManager = mock.getServerManager();
        Server createdServer = serverManager.insertOrUpdateServer(serverName, address, port).join();

        ClientManager clientManager = mock.getClientManager();
        clientManager.createClient("KarmaDev", null, null, createdServer).join();

        OfflineClient client = clientManager.getOfflineClient("KarmaDev");
        assertNotNull(client);
        assertNotNull(client.getAccount());

        Account account = client.getAccount();
        assertFalse(account.hasPassword());
        assertFalse(account.hasPin());
        assertFalse(account.hasTotpToken());
        assertFalse(account.wasTerminated());
        assertEquals(EMPTY_STR, account.getTerminator());
        assertNull(account.getTerminationDate());
        assertNotNull(account.getCreationDate());

        assertEquals(EMPTY_STR, account.getPassword());
        assertEquals(EMPTY_STR, account.getPin());
        assertEquals(EMPTY_STR, account.getTotpToken());

        Boolean passwordModified = assertDoesNotThrow(() -> account.setPassword(newPassword).join());
        Boolean pinModified = assertDoesNotThrow(() -> account.setPin(newPin).join());
        Boolean totpModified = assertDoesNotThrow(() -> account.setTotpToken(newTotp).join());
        assertTrue(passwordModified);
        assertTrue(pinModified);
        assertTrue(totpModified);

        assertEquals(newPassword, account.getPassword());
        assertEquals(newPin, account.getPin());
        assertEquals(newTotp, account.getTotpToken());
    }

    @Test
    void testThatUserSessionCanBeModified() throws UnknownHostException {
        final String serverName = "test";
        final InetAddress address = InetAddress.getByName("127.0.0.1");
        final int port = 25565;

        DatabaseConnector connector = mock.getConnector();
        connector.configure().join();

        ServerManager serverManager = mock.getServerManager();
        Server createdServer = serverManager.insertOrUpdateServer(serverName, address, port).join();

        ClientManager clientManager = mock.getClientManager();
        clientManager.createClient("KarmaDev", null, null, createdServer).join();

        OfflineClient client = clientManager.getOfflineClient("KarmaDev");
        assertNotNull(client);
        assertNotNull(client.getSession());

        Session account = client.getSession();
        assertFalse(account.isPasswordLogged());
        assertFalse(account.isPinLogged());
        assertFalse(account.isTotpLogged());

        Boolean passwordLoginChangeToTrue = assertDoesNotThrow(() -> account.setPasswordLogged(true).join());
        assertTrue(passwordLoginChangeToTrue);
        assertTrue(account.isPasswordLogged());

        Boolean passwordLoginChangeToFalse = assertDoesNotThrow(() -> account.setPasswordLogged(false).join());
        assertTrue(passwordLoginChangeToFalse);
        assertFalse(account.isPasswordLogged());

        Boolean passwordLoginChangeToCurrentStatus = assertDoesNotThrow(() -> account.setPasswordLogged(false).join());
        assertFalse(passwordLoginChangeToCurrentStatus);

        Boolean pinLoginChangeToTrue = assertDoesNotThrow(() -> account.setPinLogged(true).join());
        Boolean totpLoginChangeToTrue = assertDoesNotThrow(() -> account.setTotpLogged(true).join());

        assertTrue(pinLoginChangeToTrue);
        assertTrue(totpLoginChangeToTrue);

        Boolean pinLoginChangeToFalse = assertDoesNotThrow(() -> account.setPinLogged(false).join());
        Boolean totpLoginChangeToFalse = assertDoesNotThrow(() -> account.setTotpLogged(false).join());

        assertTrue(pinLoginChangeToFalse);
        assertTrue(totpLoginChangeToFalse);

        Boolean pinLoginChangeToCurrentState = assertDoesNotThrow(() -> account.setPinLogged(false).join());
        Boolean totpLoginChangeToCurrentState = assertDoesNotThrow(() -> account.setTotpLogged(false).join());

        assertFalse(pinLoginChangeToCurrentState);
        assertFalse(totpLoginChangeToCurrentState);
    }

    @AfterEach
    public void tearDown() throws Exception {
        mock.teardown();
    }
}
