/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.test.mock.opts.database.options;

import es.karmadev.authlock.api.plugin.file.database.OptsConnection;

public class MockConnectionOptions implements OptsConnection {

    /**
     * Get the connection host
     *
     * @return the host
     */
    @Override
    public String getHost() {
        return "127.0.0.1";
    }

    /**
     * Get the connection port
     *
     * @return the port
     */
    @Override
    public int getPort() {
        return 3306;
    }

    /**
     * Get the connection database
     *
     * @return the database to connect
     * to
     */
    @Override
    public String getDatabase() {
        return "mock_database";
    }

    /**
     * Get the connection user
     *
     * @return the user
     */
    @Override
    public String getUser() {
        return "root";
    }

    /**
     * Get the connection password
     *
     * @return the password
     */
    @Override
    public String getPassword() {
        return "root";
    }
}
