/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.util;

import org.jetbrains.annotations.Contract;

import java.lang.reflect.Array;
import java.util.Collection;

/**
 * Preconditions utility class
 */
public final class Preconditions {

    public static final String EMPTY_STR = "";

    private Preconditions() {
        //Prevent from constructing the utility class
    }

    /**
     * Assert that an object is not null
     *
     * @param object the object
     * @throws NullPointerException if the object is null
     */
    @Contract("null, null -> fail")
    public static void assertNotNull(final Object object, final String name) throws NullPointerException {
        if (name == null)
            throw new NullPointerException("name cannot be null");

        if (object == null)
            throw new NullPointerException(String.format("%s cannot be null", name));
    }

    /**
     * Assert that an object is not null
     *
     * @param object the object
     * @param name the object name
     * @param message the message
     * @throws NullPointerException if the object is null
     */
    @Contract("null, null, _ -> fail")
    public static void assertNotNull(final Object object, final String name, final String message) throws NullPointerException {
        Preconditions.assertNotNull(name, "name");
        if (object == null)
            throw new NullPointerException(message);
    }

    /**
     * Assert that an object is not empty
     *
     * @param object the object
     * @throws IllegalStateException if the object is empty
     */
    @Contract("null -> fail")
    public static void assertNotEmpty(final Object object) throws IllegalStateException {
        Preconditions.assertNotEmpty(object, null);
    }

    /**
     * Assert that an object is not empty
     *
     * @param object the object
     * @param message the message
     * @throws IllegalStateException if the object is empty
     */
    @Contract("null, _ -> fail")
    public static void assertNotEmpty(final Object object, final String message) throws IllegalStateException {
        Preconditions.assertNotNull(object, "object");

        if (object instanceof CharSequence) {
            CharSequence sequence = (CharSequence) object;
            if (sequence.length() == 0 || sequence.toString().trim()
                    .replaceAll("\\s", "").isEmpty())
                throw new IllegalStateException(message);
        } else if (object instanceof Collection) {
            Collection<?> collection = (Collection<?>) object;
            if (collection.isEmpty())
                throw new IllegalStateException(message);
        } else if (object.getClass().isArray()) {
            int length = Array.getLength(object);
            if (length == 0)
                throw new IllegalStateException(message);
        }
    }

    /**
     * Assert that an object array content is not null
     *
     * @param object the object
     * @throws IllegalStateException if any of the content is null
     */
    @Contract("null -> fail")
    public static void assertContentNotNull(final Object object) throws IllegalStateException {
        Preconditions.assertContentNotNull(object, null);
    }

    /**
     * Assert that an object is not empty
     *
     * @param object the object
     * @param message the message
     * @throws IllegalStateException if any of the content is null
     */
    @Contract("null, _ -> fail")
    public static void assertContentNotNull(final Object object, final String message) throws IllegalStateException {
        Preconditions.assertNotNull(object, "object");

        if (object instanceof CharSequence) {
            CharSequence sequence = (CharSequence) object;
            if (sequence.length() == 0)
                throw new IllegalStateException(message);
        } else if (object instanceof Iterable) {
            Iterable<?> iterable = (Iterable<?>) object;
            for (Object obj : iterable) {
                if (obj == null)
                    throw new IllegalStateException(message);
            }
        } else if (object.getClass().isArray()) {
            int length = Array.getLength(object);
            for (int i = 0; i < length; i++) {
                Object obj = Array.get(object, i);
                if (obj == null)
                    throw new IllegalStateException(message);
            }
        }
    }

    /**
     * Get an object or the default value if
     * the object is null
     *
     * @param object the object
     * @param def the default value
     * @return the object or the
     * default value
     * @param <T> the object type
     */
    public static <T> T getOrElse(final T object, final T def) {
        Preconditions.assertNotNull(def, "def");
        return object == null ? def : object;
    }
}