/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.player.client;

import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.plugin.Manager;
import es.karmadev.authlock.api.server.Server;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import es.karmadev.authlock.api.util.exception.UnsignedException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;

/**
 * General client manager
 */
@SuppressWarnings("unused")
public interface ClientManager extends Manager {

    /**
     * Create a client
     *
     * @param name the client name
     * @param online the client online-mode uuid
     * @param bedrock the client bedrock uuid
     * @param server the client server
     * @return if the client was created successfully
     */
    @NotNull
    CompletableFuture<Boolean> createClient(final @NotNull String name, final @Nullable UUID online, final @Nullable UUID bedrock,
                                            final @NotNull Server server);

    /**
     * Get all the offline clients
     *
     * @return the offline clients
     */
    Collection<OfflineClient> getOfflineClients();

    /**
     * Get all the online clients
     *
     * @return the online clients
     */
    Collection<OnlineClient> getOnlineClients();

    /**
     * Get an offline client
     *
     * @param uuid the client uuid
     * @return the offline client
     */
    @Nullable
    OfflineClient getOfflineClient(final @NotNull UUIDStore uuid);

    /**
     * Get an offline client
     *
     * @param name the client name
     * @return the offline client
     */
    @Nullable
    OfflineClient getOfflineClient(final @NotNull String name);

    /**
     * Get an offline client
     *
     * @param clientId the client id
     * @return the client
     * @throws UnsignedException if the number is signed
     */
    @Nullable
    OfflineClient getOfflineClient(final @NotNull Number clientId) throws UnsignedException;

    /**
     * Get an online client
     *
     * @param uuid the client uuid
     * @return the online client
     */
    @Nullable
    default OnlineClient getClient(final @NotNull UUIDStore uuid) {
        return this.getOnlineClients().stream().filter(c -> {
            UUIDStore store = c.getUUID();
            return store.containsAny(uuid);
        }).findAny().orElse(null);
    }

    /**
     * Get an online client
     *
     * @param name the client name
     * @return the online client
     */
    @Nullable
    default OnlineClient getClient(final @NotNull String name) {
        return this.getOnlineClients().stream()
                .filter(c -> c.getName().equals(name))
                .findAny()
                .orElse(null);
    }

    /**
     * Get an online client
     *
     * @param clientId the client id
     * @return the client
     * @throws UnsignedException if the number is signed
     */
    @Nullable
    default OnlineClient getClient(final @NotNull Number clientId) throws UnsignedException {
        UnsignedBigInteger unsignedId = new UnsignedBigInteger(clientId);
        return this.getOnlineClients().stream()
                .filter(c -> c.getEntityId().equals(unsignedId))
                .findAny()
                .orElse(null);
    }
}