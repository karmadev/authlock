/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.plugin;

import es.karmadev.api.logger.Logger;
import es.karmadev.authlock.api.auth.AuthProcessManager;
import es.karmadev.authlock.api.player.client.ClientManager;
import es.karmadev.authlock.api.plugin.extension.manager.ExtensionManager;
import es.karmadev.authlock.api.plugin.file.database.OptsDatabase;
import es.karmadev.authlock.api.server.ServerManager;
import es.karmadev.authlock.api.util.exception.plugin.NoSuchManagerException;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.nio.file.Path;

/**
 * Represents the AuthLock plugin
 */
@SuppressWarnings("unused")
public abstract class AuthLock {

    private static AuthLock instance;

    /**
     * Get the plugin database settings
     *
     * @return the database settings
     */
    @NotNull
    public abstract OptsDatabase getDatabaseOpts();

    /**
     * Get the plugin logger
     *
     * @return the plugin logger
     */
    @NotNull
    public abstract Logger getLogger();

    /**
     * Get a manager
     *
     * @param type the manager type
     * @return the manager
     * @param <T> the manager type
     * @throws NoSuchManagerException if the manager type does
     * not exist
     */
    @NotNull
    @Contract("null -> fail")
    protected abstract <T extends Manager> T getManager(final Class<T> type) throws NoSuchManagerException;

    /**
     * Get the plugin data folder
     *
     * @return the plugin data
     * folder
     */
    public abstract Path getDataFolder();

    /**
     * Request to export an internal
     * plugin resource to the specified
     * destination. If the destination is
     * a file, the resource file name will
     * be used.
     * Fail cases:
     * <ul>
     *     <li>If the resource does not exists</li>
     *     <li>If the resource is an entire directory</li>
     *     <li>If the destination file already exists</li>
     * </ul>
     * This method looks only inside "public" resources, for instance,
     * calling this method using plugin.yml as resource parameter will
     * fail unless there's a plugin.yml inside the "public" resources
     * directory
     *
     * @param resource the resource to export
     * @param destination the resource destination file
     * @return if the export was successful
     */
    public abstract boolean requestExport(final String resource, final Path destination);

    /**
     * Get the plugin client manager
     *
     * @return the client manager
     * @throws NoSuchManagerException if the client manager
     * has not been jet registered
     */
    @NotNull
    public final ClientManager getClientManager() throws NoSuchManagerException {
        return this.getManager(ClientManager.class);
    }

    /**
     * Get the plugin auth process
     * manager
     *
     * @return the auth process
     * manager
     * @throws NoSuchManagerException if the auth process manager
     * has not been jet registered
     */
    @NotNull
    public final AuthProcessManager getAuthProcessManager() throws NoSuchManagerException {
        return this.getManager(AuthProcessManager.class);
    }

    /**
     * Get the plugin extension
     * manager
     *
     * @return the extension manager
     * @throws NoSuchManagerException if the extension manager
     * has not been jet registered
     */
    @NotNull
    public final ExtensionManager getExtensionManager() throws NoSuchManagerException {
        return this.getManager(ExtensionManager.class);
    }

    /**
     * Get the plugin server
     * manager
     *
     * @return the server manager
     * @throws NoSuchManagerException if the server manager
     * has not been jet registered
     */
    @SuppressWarnings("UnusedReturnValue")
    @NotNull
    public final ServerManager getServerManager() throws NoSuchManagerException {
        return this.getManager(ServerManager.class);
    }

    /**
     * Get the authlock instance
     *
     * @return the instance of the
     * plugin
     */
    @Nullable
    public static AuthLock getInstance() {
        return AuthLock.instance;
    }

    /**
     * Set the AuthLock instance
     *
     * @throws IllegalStateException if the instance has been
     * already set
     */
    protected static void registerInstance(final AuthLock instance) throws IllegalStateException {
        if (AuthLock.instance != null)
            throw new IllegalStateException("Cannot set AuthLock instance while it has been already set");

        AuthLock.instance = instance;
    }
}
