/*
Copyright (C) 2024 AuthLock

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package es.karmadev.authlock.api.server.network;

/**
 * Represents a network message
 */
public interface NetworkMessage {

    /**
     * Get the unique message ID. The
     * message is unique in the context
     * of non-sent messages. Which means
     * if there was a message with this same
     * ID, but the message was already sent
     * and confirmed by the peer, the message
     * doesn't necessarily need to be the same
     * as the previously sent
     *
     * @return the message ID
     */
    long getMessageId();

    /**
     * Get the raw message data. According to
     * the AuthLock specification, network messages
     * are always json messages, so the raw message
     * data are just the bytes which compose the
     * json string
     * <code>
     *     byte[] data = getRawData();
     *     String jsonString = new String(data);
     *     System.out.println(jsonString);
     *     <br>
     *     >> Prints something like {"id": 1, "origin": "127.0.0.1:25565", ...}
     * </code>
     *
     * @return the raw message data
     */
    byte[] getRawData();
}