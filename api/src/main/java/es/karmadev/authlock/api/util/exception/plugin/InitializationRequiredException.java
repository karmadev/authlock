package es.karmadev.authlock.api.util.exception.plugin;

/**
 * This exception is thrown when the {@link es.karmadev.authlock.api.plugin.AuthLock} plugin
 * is required, but it has not been yet initialized. Those exceptions shouldn't be raised
 * unless an external module tries to access a resource which requires of the
 * plugin before that has been initialized
 */
public class InitializationRequiredException extends RuntimeException {

    /**
     * Create the exception
     *
     * @param clazz the class the exception is being
     *              thrown from
     * @param method the method that has been tried to
     *               be run
     */
    public InitializationRequiredException(final Class<?> clazz, final String method) {
        super(String.format("Cannot access %s#%s because AuthLock has not been yet initialized",
                clazz.getSimpleName(), method));
    }
}
