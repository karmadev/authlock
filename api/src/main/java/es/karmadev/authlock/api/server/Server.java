/*
Copyright (C) 2024 AuthLock

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package es.karmadev.authlock.api.server;

import es.karmadev.authlock.api.player.client.OfflineClient;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.sequel.entity.SQLEntity;
import es.karmadev.authlock.api.server.network.MessageTimeoutException;
import es.karmadev.authlock.api.server.network.NetworkMessage;

import java.net.InetAddress;
import java.util.Collection;
import java.util.concurrent.*;
import java.util.stream.Collectors;

/**
 * Represents a server. The server represents
 * nothing but the current client server. It is
 * not a direct server API
 */
@SuppressWarnings("unused")
public interface Server extends SQLEntity {

    /**
     * Get the server name
     *
     * @return the server name
     */
    String getName();

    /**
     * Get the server address
     *
     * @return the server address
     */
    InetAddress getAddress();

    /**
     * Get the server port
     *
     * @return the server port
     */
    int getPort();

    /**
     * Get if the server is currently
     * connected
     *
     * @return if the server is currently
     * connected to the network. This value
     * is directly provided by a constant ping
     * from the network to the server every
     * ten seconds.
     */
    boolean isConnected();

    /**
     * Get all the offline clients in the
     * server.
     * The offline clients may also
     * contain clients which are currently
     * online.
     * This list also represents the
     * clients who disconnected in this server
     *
     * @return the clients in the server
     */
    Collection<OfflineClient> getOfflineClients();

    /**
     * Get all the online clients in the
     * server.
     *
     * @return the clients in the server
     */
    default Collection<OnlineClient> getOnlineClients() {
        return this.getOfflineClients()
                .stream()
                .filter(OfflineClient::isConnected)
                .map(OfflineClient::getOnlineClient)
                .collect(Collectors.toList());
    }

    /**
     * Get if the client is connected
     * to this server
     *
     * @param client the client
     * @return if the client is connected
     */
    default boolean isConnected(final OfflineClient client) {
        return this.getOfflineClients().contains(client);
    }

    /**
     * Send a message to the server. Is highly recommended
     * to use {@link #sendMessage(NetworkMessage, long)} or {@link #sendMessage(NetworkMessage, long, TimeUnit)}
     * as those will time out the message task. This method
     * will keep the task waiting until a response or an exception
     * is provided.
     *
     * @param message the message to send
     * @return the message task
     */
    CompletableFuture<Boolean> sendMessage(final NetworkMessage message);

    /**
     * Send a message to the server
     *
     * @param message the message to send
     * @param timeout the message timeout, in milliseconds
     * @return the message task
     */
    default CompletableFuture<Boolean> sendMessage(final NetworkMessage message, final long timeout) {
        return this.sendMessage(message, Math.max(1, timeout), TimeUnit.MILLISECONDS);
    }

    /**
     * Send a message to the server
     *
     * @param message the message to send
     * @param timeout the message timeout
     * @param timeUnit the message timeout time unit
     * @return the message task
     */
    @SuppressWarnings("resource")
    default CompletableFuture<Boolean> sendMessage(final NetworkMessage message, final long timeout, final TimeUnit timeUnit) {
        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);

        CompletableFuture<Boolean> future = new CompletableFuture<>();
        this.sendMessage(message)
                .thenAccept(result -> {
                    if (future.isCancelled() || future.isDone() || future.isCompletedExceptionally())
                        throw new MessageTimeoutException(message.getMessageId());

                    future.complete(result);
                    scheduler.shutdownNow();
                }).exceptionally(error -> {
                    if (future.isCancelled() || future.isDone() || future.isCompletedExceptionally())
                        throw new MessageTimeoutException(message.getMessageId(), error);

                    future.completeExceptionally(error);
                    scheduler.shutdownNow();

                    return null;
                });

        scheduler.schedule(() -> {
            if (future.isCancelled() || future.isDone() || future.isCompletedExceptionally())
                return;

            future.completeExceptionally(new MessageTimeoutException());
            scheduler.shutdownNow();
        }, Math.max(1, timeout), timeUnit);

        return future;
    }
}