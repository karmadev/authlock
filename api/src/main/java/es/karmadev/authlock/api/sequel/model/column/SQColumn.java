package es.karmadev.authlock.api.sequel.model.column;

import es.karmadev.authlock.api.sequel.model.table.SQTable;

public final class SQColumn {

    public static final SQColumn IDENTIFIER = new SQColumn("Identifier");
    public static final SQColumn USERNAME = new SQColumn("Username");
    public static final SQColumn NAME = new SQColumn("Name");
    public static final SQColumn UUID = new SQColumn("UniqueId");
    public static final SQColumn ACCOUNT = new SQColumn("Account");
    public static final SQColumn SESSION = new SQColumn("Session");
    public static final SQColumn SERVER = new SQColumn("Server");
    public static final SQColumn ADDRESS = new SQColumn("Address");
    public static final SQColumn PORT = new SQColumn("Port");
    public static final SQColumn PASSWORD = new SQColumn("Password");
    public static final SQColumn PIN = new SQColumn("Pin");
    public static final SQColumn TOTP = new SQColumn("Totp");
    public static final SQColumn VALIDATION = new SQColumn("Validation");
    public static final SQColumn TERMINATOR = new SQColumn("Terminator");
    public static final SQColumn TERMINATION = new SQColumn("Termination");
    public static final SQColumn OFFLINE = new SQColumn("Offline");
    public static final SQColumn ONLINE = new SQColumn("Online");
    public static final SQColumn BEDROCK = new SQColumn("Bedrock");
    public static final SQColumn ACTIVE = new SQColumn("Active");
    public static final SQColumn CREATION = new SQColumn("Creation");

    private final String sqColumnName;

    /**
     * Create a new column
     *
     * @param sqColumnName the column name
     */
    public SQColumn(final String sqColumnName) {
        this.sqColumnName = sqColumnName;
    }

    /**
     * Get the column name. Similarly, as
     * {@link SQTable#getName()} method, this
     * only returns the internal name of
     * the column, even thought in the database
     * server might look the same
     *
     * @return the column name
     */
    public String getName() {
        return sqColumnName;
    }
}
