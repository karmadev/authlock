/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.player.client;

import es.karmadev.authlock.api.authority.AuthorityEntity;
import es.karmadev.authlock.api.player.UUIDStore;
import es.karmadev.authlock.api.player.account.Account;
import es.karmadev.authlock.api.player.session.Session;
import es.karmadev.authlock.api.sequel.entity.SQLEntity;
import es.karmadev.authlock.api.server.Server;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * Represents a client which is
 * currently not in the server
 */
@SuppressWarnings("unused")
public interface OfflineClient extends AuthorityEntity, SQLEntity {

    /**
     * Get the client name
     *
     * @return the client name
     */
    @NotNull
    String getName();

    /**
     * Set the account name
     *
     * @param name the account name
     * @return if the action was successful
     */
    @NotNull @Contract("null -> fail")
    CompletableFuture<Boolean> setName(final String name);

    /**
     * Get the client UUID store
     *
     * @return the UUID store
     */
    @NotNull
    UUIDStore getUUID();

    /**
     * Set the account UUID data
     *
     * @param uuid the uuid data
     * @return if the action was successful
     */
    @NotNull @Contract("null -> fail")
    CompletableFuture<Boolean> setUUID(final UUIDStore uuid);

    /**
     * Get the online client of this
     * offline client
     *
     * @return the online client
     */
    @Nullable
    OnlineClient getOnlineClient();

    /**
     * Get the client account
     *
     * @return the client account
     */
    @NotNull
    Account getAccount();

    /**
     * Get the client session
     *
     * @return the client session
     */
    @NotNull
    Session getSession();

    /**
     * Get the client server
     *
     * @return the client server
     */
    Server getServer();

    /**
     * Set the client server
     *
     * @param server the new server
     * @return if the action was successful
     */
    CompletableFuture<Boolean> setServer(final Server server);

    /**
     * Get if the client is
     * currently connected
     *
     * @return if the client is
     * connected
     */
    boolean isConnected();

    /**
     * Get when the client was created
     * in the database
     *
     * @return when the client was created
     */
    Instant getCreation();
}