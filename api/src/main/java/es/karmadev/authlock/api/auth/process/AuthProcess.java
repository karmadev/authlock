/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.auth.process;

import es.karmadev.authlock.api.auth.process.step.AuthResult;
import es.karmadev.authlock.api.auth.process.step.StepResult;
import es.karmadev.authlock.api.auth.process.step.StepTarget;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.concurrent.CompletableFuture;

/**
 * Represents an auth process of
 * a single user
 */
public final class AuthProcess {

    private final OnlineClient client;
    private final AuthStep[] steps;
    private int currentStep = 0;

    /**
     * Create a new auth process
     *
     * @param steps the process steps
     */
    public AuthProcess(final @NotNull OnlineClient client, final @NotNull AuthStep... steps) {
        Preconditions.assertNotNull(client, "client");
        Preconditions.assertContentNotNull(steps);
        Preconditions.assertNotEmpty(steps);

        this.client = client;
        this.steps = steps;
    }

    /**
     * Get the number of auth
     * steps
     *
     * @return the step amount
     */
    public int getSteps() {
        return this.steps.length;
    }

    /**
     * Get the number of the
     * current auth step
     *
     * @return the step id
     */
    public int getStep() {
        return this.currentStep;
    }

    /**
     * Get the current auth step
     *
     * @return the auth step
     */
    public AuthStep getCurrentStep() {
        assert this.steps.length > 0;
        assert this.currentStep < this.steps.length;

        return this.steps[this.currentStep];
    }

    /**
     * Get a step by the step number. Providing
     * a number higher than the current step number
     * will cause this to return null, as only run
     * steps can be retrieved
     *
     * @param step the step
     * @return the step
     */
    @Nullable
    public AuthStep getCompletedStep(final int step) {
        if (step < 0 || step >= this.currentStep)
            return null;

        return this.steps[step];
    }

    /**
     * Get if the auth process has
     * finished
     *
     * @return if the process is finished
     */
    public boolean isFinished() {
        if (this.getStep() != this.getSteps())
            return false;

        AuthStep last = this.getCurrentStep();
        return last != null && last.isFinished();
    }

    /**
     * Execute the process
     *
     * @return the final result
     * @throws InitializationRequiredException if the authlock plugin has not been
     * yet initialized
     */
    public CompletableFuture<StepResult> execute() throws InitializationRequiredException {
        if (this.isFinished()) {
            return CompletableFuture.completedFuture(
                    this.getCurrentStep().getResult()
            );
        }

        return this.executeStep();
    }

    private CompletableFuture<StepResult> executeStep() throws InitializationRequiredException {
        AuthLock plugin = AuthLock.getInstance();
        if (plugin == null)
            throw new InitializationRequiredException(AuthProcess.class, "executeStep");

        AuthStep step = this.getCurrentStep();
        if (this.currentStep >= this.steps.length - 1)
            return CompletableFuture.completedFuture(step.getResult());

        return step.process(client).thenCompose(result -> {
            if (result == null) {
                return CompletableFuture.completedFuture(
                        AuthResult.DENIED.reason("Unexpected auth result on " + step.getClass().getName())
                );
            }

            if (!result.getResult().equals(AuthResult.ALLOWED))
                return CompletableFuture.completedFuture(result);

            if (++this.currentStep == this.steps.length) {
                --this.currentStep; //Reset the current step
                return CompletableFuture.completedFuture(result);
            }

            return executeStep();
        }).exceptionally(exception -> {
            plugin.getLogger().error(exception, "Failed to execute auth process for {}", client.getName());
            return AuthResult.DENIED.reason("An exception occurred");
        });
    }

    /**
     * Create a completed auth process
     *
     * @return a completed process
     */
    public static AuthProcess completedProcess(final OnlineClient client) {
        return new AuthProcess(client, DummyCompletedStep.createInstance());
    }
}

class DummyCompletedStep implements AuthStep {

    private DummyCompletedStep() {
        //Prevent from building using constructor
    }

    /**
     * Process the step
     *
     * @param client the client being
     *               processed
     * @return when the process has completed
     */
    @Override
    public CompletableFuture<StepResult> process(final OnlineClient client) {
        return CompletableFuture.completedFuture(
                AuthResult.ALLOWED.reason("")
        );
    }

    /**
     * Get if the step is finished
     *
     * @return if finished
     */
    @Override
    public boolean isFinished() {
        return true;
    }

    /**
     * Get the step result
     *
     * @return the result
     */
    @Override
    public StepResult getResult() {
        return AuthResult.ALLOWED.reason("");
    }

    /**
     * Get the step target
     *
     * @return the step target
     */
    @Override
    public StepTarget getTarget() {
        return StepTarget.LOGIN;
    }

    /**
     * Get the completed step
     * instance
     *
     * @return the completed instance
     */
    public static DummyCompletedStep createInstance() {
        return new DummyCompletedStep();
    }
}