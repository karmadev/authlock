/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.player.account;

import es.karmadev.authlock.api.authority.AuthorityEntity;
import es.karmadev.authlock.api.authority.AuthorityObject;
import es.karmadev.authlock.api.authority.exception.AuthorizationException;
import es.karmadev.authlock.api.sequel.entity.SQLEntity;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * Represents an account
 */
public interface Account extends AuthorityObject, SQLEntity {

    /**
     * Get the authority object
     * name
     *
     * @return the authority
     * object name
     */
    @Override
    default String getObjectName() {
        return "Account";
    }

    /**
     * Get the client of the account
     *
     * @return the account client
     */
    UnsignedBigInteger getClientId();

    /**
     * Get the account password
     *
     * @return the account password
     */
    @NotNull
    String getPassword();

    /**
     * Get if the account has a
     * password
     *
     * @return if the account has the
     * password defined
     */
    boolean hasPassword();

    /**
     * Set the account password
     *
     * @param newPassword the new account password
     * @return if the password was set
     */
    CompletableFuture<Boolean> setPassword(final @NotNull String newPassword);

    /**
     * Get the account pin
     *
     * @return the account pin
     */
    @NotNull
    String getPin();

    /**
     * Get if the account has a
     * pin
     *
     * @return if the account has a pin
     */
    boolean hasPin();

    /**
     * Set the account pin
     *
     * @param newPin the new pin
     * @return if the pin was set
     */
    CompletableFuture<Boolean> setPin(final @NotNull String newPin);

    /**
     * Get the client totp token
     *
     * @return the client totp token
     */
    @NotNull
    String getTotpToken();

    /**
     * Get if the client has the
     * totp token defined
     *
     * @return if the client has a totp
     * token
     */
    boolean hasTotpToken();

    /**
     * Set the account totp token
     *
     * @param newTotpToken the new totp
     *                     token
     * @return if the token was set
     */
    CompletableFuture<Boolean> setTotpToken(final @NotNull String newTotpToken);

    /**
     * Delete the account
     *
     * @param authority the account termination
     *                  issuer
     * @return if the account was successfully
     * removed
     * @throws AuthorizationException if the authority does
     * not have power over the account
     */
    CompletableFuture<Boolean> terminate(final @NotNull AuthorityEntity authority) throws AuthorizationException;

    /**
     * Get if the account was terminated
     *
     * @return if the account was
     * terminated
     */
    boolean wasTerminated();

    /**
     * Get the account terminator
     *
     * @return the terminator
     * of the account
     */
    @NotNull
    String getTerminator();

    /**
     * Get when the client account
     * was created
     *
     * @return the account creation time
     */
    @NotNull
    Instant getCreationDate();

    /**
     * Get when the client account
     * was terminated
     *
     * @return the account termination time
     */
    @Nullable
    Instant getTerminationDate();
}