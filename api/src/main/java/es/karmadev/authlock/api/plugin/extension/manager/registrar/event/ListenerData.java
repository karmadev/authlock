/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.plugin.extension.manager.registrar.event;

import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.plugin.extension.event.AuthLockEvent;
import es.karmadev.authlock.api.plugin.extension.event.AuthLockListener;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;

import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 * Represents data about a
 * listener
 */
final class ListenerData {

    private static final AuthLock plugin = AuthLock.getInstance();
    private final Map<AuthLockListener, Collection<Method>> eventCallers = new HashMap<>();

    /**
     * Append all the suitable event method
     * catchers on the provided listener
     *
     * @param listener the listener
     * @param eventClass the event class to catch
     */
    public void append(final AuthLockListener listener, final Class<? extends AuthLockEvent> eventClass) {
        this.eventCallers.compute(listener, (k, v) -> {
            if (v == null)
                v = new HashSet<>();

            Method[] listenerMethods = listener.getClass().getMethods();
            for (Method method : listenerMethods) {
                Class<?>[] params = method.getParameterTypes();
                if (params.length != 1) continue;

                Class<?> parameter = params[0];
                if (eventClass.equals(parameter))
                    v.add(method);
            }

            return v;
        });
    }

    /**
     * Remove all the suitable event methods
     * known for the provided listener
     *
     * @param listener the listener
     */
    public void remove(final AuthLockListener listener) {
        Preconditions.assertNotNull(listener, "listener");
        this.eventCallers.remove(listener);
    }

    /**
     * Get if the data is empty
     *
     * @return if there are no suitable
     * methods
     */
    public boolean isEmpty() {
        return this.eventCallers.isEmpty();
    }

    /**
     * Invoke all the methods in the
     * listeners
     *
     * @param event the event to fire
     * @param <T> the event type
     */
    public <T extends AuthLockEvent> void invokeAll(final T event) {
        Preconditions.assertNotNull(event, "event");
        if (plugin == null)
            throw new InitializationRequiredException(ListenerData.class, "invokeAll(AuthLockEvent)");

        for (Map.Entry<AuthLockListener, Collection<Method>> entry : this.eventCallers.entrySet()) {
            Collection<Method> invokers = entry.getValue();
            if (invokers == null || invokers.isEmpty())
                continue;

            for (Method invoker : invokers)
                tryInvokeEvent(entry.getKey(), invoker, event);
        }
    }

    private void tryInvokeEvent(final AuthLockListener listener, final Method method, final AuthLockEvent event) {
        try {
            method.invoke(listener, event);
        } catch (ReflectiveOperationException ex) {
            assert plugin != null;
            plugin.getLogger().error(ex, "Failed to invoke event {}", event.getClass().getSimpleName());
        }
    }
}