/*
Copyright (C) 2024 AuthLock

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package es.karmadev.authlock.api.server;

import es.karmadev.authlock.api.plugin.Manager;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;

/**
 * Represents a server manager
 */
@SuppressWarnings("unused")
public interface ServerManager extends Manager {

    /**
     * Creates a new server or updates
     * an existing one
     *
     * @param name the server name
     * @param address the server address
     * @param port the server port
     * @return the updated server
     */
    CompletableFuture<Server> insertOrUpdateServer(
            final String name,
            final InetAddress address,
            final int port
    );

    /**
     * Get all the existing servers
     *
     * @return the existing server
     */
    Collection<Server> getServers();

    /**
     * Get a server by its id
     *
     * @param id the server id
     * @return the server
     */
    default Server getServer(final UnsignedBigInteger id) {
        return this.getServers()
                .stream()
                .filter(s -> s.getEntityId().equals(id))
                .findAny().orElse(null);
    }

    /**
     * Get a server by its id
     *
     * @param id the server id
     * @return the server
     */
    default Server getServer(final Number id) {
        return this.getServer(new UnsignedBigInteger(id));
    }

    /**
     * Get a server by name
     *
     * @param name the server name
     * @return the server
     */
    default Server getServer(final String name) {
        return this.getServers().stream()
                .filter(s -> s.getName().equals(name))
                .findAny().orElse(null);
    }

    /**
     * Get a server by address
     *
     * @param address the server address
     * @param port the server port
     * @return the server
     */
    default Server getServer(final InetAddress address, final int port) {
        return this.getServers().stream()
                .filter(s -> s.getAddress().equals(address))
                .filter(s -> s.getPort() == port)
                .findAny().orElse(null);
    }

    /**
     * Get a server by address
     *
     * @param address the server address
     * @return the server
     */
    default Server getServer(final InetSocketAddress address) {
        return this.getServer(address.getAddress(), address.getPort());
    }
}
