package es.karmadev.authlock.api.sequel.model.table;

import es.karmadev.api.kyle.yaml.YamlContent;
import es.karmadev.api.kyle.yaml.exception.YamlLoadException;
import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.sequel.model.column.SQColumn;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

/**
 * Sequel table
 */
public abstract class SQTable {

    protected final String name;

    protected SQTable(final String name) {
        this.name = name;
    }

    /**
     * Gets the internal name of the
     * SQL table. This name does not
     * provide the actual name of
     * the table in the SQL server,
     * even though they might be the
     * same
     *
     * @return the table name
     */
    public final String getName() {
        return this.name;
    }

    /**
     * Get the table name
     *
     * @return the table name
     */
    public abstract String getTableName();

    /**
     * Get a column name
     *
     * @param column the column
     * @return the column name
     */
    public abstract String getColumnName(final SQColumn column);

    /**
     * Get the table supported columns
     *
     * @return the table columns
     */
    public abstract SQColumn[] getColumns();

    /**
     * Gets the account table
     *
     * @return the account table
     */
    public static SQTable account() {
        return YamlSchemedSQLTable.ACCOUNTS;
    }

    /**
     * Gets the session table
     *
     * @return the session table
     */
    public static SQTable session() {
        return YamlSchemedSQLTable.SESSIONS;
    }

    /**
     * Gets the server table
     *
     * @return the server table
     */
    public static SQTable server() {
        return YamlSchemedSQLTable.SERVERS;
    }

    /**
     * Gets the uuid table
     *
     * @return the uuid table
     */
    public static SQTable uuid() {
        return YamlSchemedSQLTable.UUIDS;
    }

    /**
     * Gets the user table
     *
     * @return the user table
     */
    public static SQTable user() {
        return YamlSchemedSQLTable.USERS;
    }
}

class YamlSchemedSQLTable extends SQTable {

    /**
     * Accounts table
     */
    protected static final YamlSchemedSQLTable ACCOUNTS = new YamlSchemedSQLTable("accounts",
            SQColumn.IDENTIFIER,
            SQColumn.PASSWORD,
            SQColumn.PIN,
            SQColumn.TOTP,
            SQColumn.TERMINATOR,
            SQColumn.TERMINATION,
            SQColumn.CREATION
    );

    /**
     * Sessions table
     */
    protected static final YamlSchemedSQLTable SESSIONS = new YamlSchemedSQLTable("sessions",
            SQColumn.IDENTIFIER,
            SQColumn.PASSWORD,
            SQColumn.PIN,
            SQColumn.TOTP,
            SQColumn.VALIDATION
    );

    /**
     * Servers table
     */
    protected static final YamlSchemedSQLTable SERVERS = new YamlSchemedSQLTable("servers",
            SQColumn.IDENTIFIER,
            SQColumn.NAME,
            SQColumn.ADDRESS,
            SQColumn.PORT
    );

    /**
     * UUIDs table
     */
    protected static final YamlSchemedSQLTable UUIDS = new YamlSchemedSQLTable("uuids",
            SQColumn.IDENTIFIER,
            SQColumn.OFFLINE,
            SQColumn.ONLINE,
            SQColumn.BEDROCK,
            SQColumn.ACTIVE
    );

    /**
     * Users table
     */
    protected static final YamlSchemedSQLTable USERS = new YamlSchemedSQLTable("users",
            SQColumn.IDENTIFIER,
            SQColumn.USERNAME,
            SQColumn.UUID,
            SQColumn.ACCOUNT,
            SQColumn.SESSION,
            SQColumn.SERVER,
            SQColumn.ADDRESS,
            SQColumn.CREATION
    );

    /**
     * Yaml content
     * Why final?
     * Tables schema shouldn't be modified during
     * plugin runtime...
     */
    private final YamlContent yamlContent;

    private final Collection<SQColumn> columns = new HashSet<>();

    protected YamlSchemedSQLTable(final String name, final SQColumn... columns) {
        super(name);
        this.yamlContent = loadYaml();
        this.columns.addAll(Arrays.asList(columns == null ? new SQColumn[0] : columns));
    }

    /**
     * Get the table name
     *
     * @return the table name
     * @throws InitializationRequiredException if the plugin has not
     * been yet initialized
     */
    @Override
    public String getTableName() throws InitializationRequiredException {
        return this.yamlContent.getString("TableName", this.name);
    }

    /**
     * Get a column name
     *
     * @param column the column
     * @return the column name
     */
    @Override
    public String getColumnName(final SQColumn column) {
        return this.yamlContent.getString(String.format("Columns.%s", column.getName()), column.getName()
                .toLowerCase());
    }

    /**
     * Get the table supported columns
     *
     * @return the table columns
     */
    @Override
    public SQColumn[] getColumns() {
        return this.columns.toArray(new SQColumn[0]);
    }

    private YamlContent loadYaml() {
        AuthLock plugin = AuthLock.getInstance();
        if (plugin == null)
            throw new InitializationRequiredException(SQTable.class, "getTableName");

        final String fileName = String.format("%s.yml", this.name);

        Path dataFolder = plugin.getDataFolder();
        Path tableConfiguration = dataFolder.resolve("table").resolve(fileName);
        if (!Files.exists(tableConfiguration) &&
                !plugin.requestExport(
                        String.format("sequel/tables/%s", fileName),
                        tableConfiguration
                ))
            plugin.getLogger().warning("Failed to export {}. Default values will be used", fileName);

        try (InputStream stream = SQTable.class.getResourceAsStream(String.format("/public/sequel/tables/%s", fileName))) {
            YamlContent content = YamlContent.load(tableConfiguration, stream);
            content.validate();

            return content;
        } catch (IOException ex) {
            throw new YamlLoadException(ex);
        }
    }
}