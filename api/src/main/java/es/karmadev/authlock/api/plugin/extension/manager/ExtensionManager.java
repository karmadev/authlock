/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.plugin.extension.manager;

import es.karmadev.authlock.api.plugin.AuthLock;
import es.karmadev.authlock.api.plugin.Manager;
import es.karmadev.authlock.api.plugin.extension.AuthLockExtension;
import es.karmadev.authlock.api.plugin.extension.event.AuthLockListener;
import es.karmadev.authlock.api.plugin.extension.manager.registrar.event.ExtensionEventRegistrar;
import es.karmadev.authlock.api.util.Preconditions;
import es.karmadev.authlock.api.util.exception.plugin.InitializationRequiredException;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;

@SuppressWarnings("unused")
public final class ExtensionManager implements Manager {

    private static final AuthLock plugin = AuthLock.getInstance();
    private final Collection<AuthLockExtension> loaded = ConcurrentHashMap.newKeySet();
    private final Map<AuthLockExtension, ExtensionEventRegistrar> eventRegistrarMap = new ConcurrentHashMap<>();

    /**
     * Register a extension
     *
     * @param extension the extension to register
     * @return if the extension was
     * registered
     */
    public boolean register(final AuthLockExtension extension) {
        Preconditions.assertNotNull(extension, "extension");
        if (plugin == null)
            throw new InitializationRequiredException(ExtensionManager.class, "register(AuthLockExtension)");

        AuthLockExtension existing = this.getExtension(extension.getName());
        if (existing != null) {
            String existingVersion = existing.getVersion();
            String currentVersion = extension.getVersion();

            if (existingVersion.equalsIgnoreCase(currentVersion)) {
                plugin.getLogger().severe("Extension {} already registered", extension.getName());
            } else {
                plugin.getLogger().severe("Redundant extension name {} with different versions. Loaded {}, tried to load {}",
                        extension.getName(),
                        existing.getVersion(),
                        extension.getVersion());
            }

            return false;
        }

        if (this.loaded.add(extension)) {
            this.eventRegistrarMap.put(extension, new ExtensionEventRegistrar());
            return true;
        }

        return false;
    }

    /**
     * Unregisters an extension from the
     * extension manager
     *
     * @param extension the extension to register
     * @return if the extension was registered
     */
    public boolean unregister(final AuthLockExtension extension) {
        Preconditions.assertNotNull(extension, "extension");
        if (plugin == null)
            throw new InitializationRequiredException(ExtensionManager.class, "unregister(AuthLockExtension)");

        if (this.loaded.remove(extension)) {
            this.eventRegistrarMap.remove(extension);
            return true;
        }

        return false;
    }

    /**
     * Registers all the events on the provided
     * listener to the specified extension
     *
     * @param extension the extension
     * @param listener the event listener
     */
    public void registerListeners(final AuthLockExtension extension, final AuthLockListener listener) {
        AuthLockExtension existing = this.getExtension(extension.getName());
        if (existing == null)
            throw new IllegalStateException(extension.getName() + " tried to register listener while not registered");

        ExtensionEventRegistrar registrar = this.eventRegistrarMap.computeIfAbsent(existing, e -> new ExtensionEventRegistrar());
        registrar.register(listener);
    }

    /**
     * Unregister all the events on the provided
     * listener from the specified extension
     *
     * @param extension the extension
     * @param listener the event listener
     */
    public void unregisterListeners(final AuthLockExtension extension, final AuthLockListener listener) {
        AuthLockExtension existing = this.getExtension(extension.getName());
        if (existing == null)
            throw new IllegalStateException(extension.getName() + " tried to unregister listeners while not registered");

        ExtensionEventRegistrar registrar = this.eventRegistrarMap.computeIfAbsent(existing, e -> new ExtensionEventRegistrar());
        registrar.remove(listener);
    }

    /**
     * Get if an extension is loaded
     *
     * @param extension the extension class
     * @return if the extension is loaded
     * @param <T> the extension type
     */
    public <T extends AuthLockExtension> boolean hasExtension(final Class<T> extension) {
        return loaded.stream().anyMatch(ext -> ext.getClass().equals(extension));
    }

    /**
     * Get if an extension is loaded
     *
     * @param name the extension name
     * @return if the extension is loaded
     */
    public boolean hasExtension(final String name) {
        return loaded.stream().anyMatch(ext -> ext.getName().equalsIgnoreCase(name));
    }

    /**
     * Get an extension
     *
     * @param extension the extension class
     * @return the extension
     * @param <T> the extension type
     */
    @SuppressWarnings("unchecked")
    public <T extends AuthLockExtension> T getExtension(final Class<T> extension) {
        return (T) loaded.stream().filter(ext -> ext.getClass().equals(extension))
                .findAny().orElse(null);
    }

    /**
     * Get an extension
     *
     * @param name the extension name
     * @return the extension
     */
    public AuthLockExtension getExtension(final String name) {
        return loaded.stream().filter(ext -> ext.getName().equalsIgnoreCase(name))
                .findAny().orElse(null);
    }

    /**
     * Prepares the object manager
     * cache
     */
    @Override
    public @NotNull CompletableFuture<Void> prepareCache() {
        return CompletableFuture.completedFuture(null);
    }
}
