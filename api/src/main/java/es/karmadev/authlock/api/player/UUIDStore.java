/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.player;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;
import java.util.UUID;

/**
 * Represents a player UUID store
 */
@SuppressWarnings("unused")
public final class UUIDStore {

    private final UUID offline;
    private final UUID premium;
    private final UUID bedrock;

    /**
     * Create a UUID store
     *
     * @param offline the offline account ID
     * @param premium the premium account ID
     * @param bedrock the bedrock account ID
     * @throws IllegalStateException if all the UUIDs are null
     */
    @Contract("null, null, null -> fail")
    public UUIDStore(final @Nullable UUID offline, final @Nullable UUID premium, final @Nullable UUID bedrock) throws IllegalStateException {
        if (offline == null && premium == null && bedrock == null)
            throw new IllegalStateException("Cannot create UUID store for all null UUIDs");

        this.offline = offline;
        this.premium = premium;
        this.bedrock = bedrock;
    }

    /**
     * Get the offline UUID
     *
     * @return the offline-mode UUID
     */
    @Nullable
    public UUID getOffline() {
        return this.offline;
    }

    /**
     * Get the online UUID
     *
     * @return the online-mode UUID
     */
    @Nullable
    public UUID getPremium() {
        return this.premium;
    }

    /**
     * Get the bedrock UUID
     *
     * @return the bedrock UUID
     */
    @Nullable
    public UUID getBedrock() {
        return this.bedrock;
    }

    /**
     * Get if the uuid store
     * offline UUID is valid
     *
     * @return if the offline UUID is valid
     */
    public boolean hasOffline() {
        return this.offline != null;
    }

    /**
     * Get if the uuid store
     * premium UUID is valid
     *
     * @return if the premium UUID is valid
     */
    public boolean hasPremium() {
        return this.premium != null;
    }

    /**
     * Get if the uuid store
     * bedrock UUID is valid
     *
     * @return if the bedrock UUID is valid
     */
    public boolean hasBedrock() {
        return this.bedrock != null;
    }

    /**
     * Get if the uuid is contained within this
     * UUID store
     *
     * @param uuid the uuid
     * @return if the uuid is contained
     */
    public boolean contains(final UUID uuid) {
        if (uuid == null) return false;
        return uuid.equals(this.offline) ||
                uuid.equals(this.premium) ||
                uuid.equals(this.bedrock);
    }

    /**
     * Get if the uuid store contains any
     * of the current UUID store uuids
     *
     * @param other the other uuid store
     * @return if the object contains any of the
     * uuids
     */
    public boolean containsAny(final UUIDStore other) {
        if (other == null) return false;
        return other.contains(this.offline) ||
                other.contains(this.premium) ||
                other.contains(this.bedrock);
    }

    @Override
    public boolean equals(final Object object) {
        if (this == object) return true;
        if (!(object instanceof UUIDStore)) return false;

        UUIDStore uuidStore = (UUIDStore) object;
        return Objects.equals(getOffline(), uuidStore.getOffline()) &&
                Objects.equals(getPremium(), uuidStore.getPremium()) &&
                Objects.equals(getBedrock(), uuidStore.getBedrock());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getOffline(), getPremium(), getBedrock());
    }

    /**
     * Create a UUID store of
     * an offline mode UUID
     *
     * @param uuid the uuid
     * @return the UUID store
     */
    public static UUIDStore offlineId(final @NotNull UUID uuid) {
        return new UUIDStore(uuid, null, null);
    }

    /**
     * Create a UUID store of
     * an online mode UUID
     *
     * @param uuid the uuid
     * @return the UUID store
     */
    public static UUIDStore onlineId(final @NotNull UUID uuid) {
        return new UUIDStore(null, uuid, null);
    }

    /**
     * Create a UUID store of
     * an offline mode and/or
     * online mode uuids
     *
     * @param offline the offline uuid
     * @param online the online uuid
     * @return the uuid store
     */
    @Contract("null, null -> fail")
    public static UUIDStore fullId(final @Nullable UUID offline, final @Nullable UUID online) {
        return new UUIDStore(offline, online, null);
    }

    /**
     * Create a UUID store of
     * a bedrock mode UUID
     *
     * @param uuid the uuid
     * @return the UUID store
     */
    public static UUIDStore bedrockId(final @NotNull UUID uuid) {
        return new UUIDStore(null, null, uuid);
    }

    /**
     * Create a UUID store for the
     * provided UUID strings
     *
     * @param uuidOffline the offline UUID
     * @param uuidOnline the online UUID
     * @param uuidBedrock the bedrock UUID
     * @return the uuid store
     */
    public static UUIDStore compose(final String uuidOffline, final String uuidOnline, final String uuidBedrock) {
        UUID offline = getUUID(uuidOffline);
        UUID online = getUUID(uuidOnline);
        UUID bedrock = getUUID(uuidBedrock);

        return new UUIDStore(offline, online, bedrock);
    }

    private static UUID getUUID(final String uuid) {
        try {
            return UUID.fromString(fromTrimmed(uuid));
        } catch (IllegalArgumentException | NullPointerException ignored) {
            //ignored
        }
        return null;
    }

    private static String fromTrimmed(final String uuid) {
        if (uuid == null || uuid.trim().length() != 32)
            return uuid;

        StringBuilder dashedUUID = new StringBuilder(uuid);
        dashedUUID.insert(8, '-');
        dashedUUID.insert(13, '-');
        dashedUUID.insert(18, '-');
        dashedUUID.insert(23, '-');

        return dashedUUID.toString();
    }
}