/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.plugin.extension.manager.registrar.event;

import es.karmadev.authlock.api.plugin.extension.event.AuthLockEvent;
import es.karmadev.authlock.api.plugin.extension.event.AuthLockListener;
import es.karmadev.authlock.api.util.Preconditions;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Represents an extension event
 * registrar
 */
@SuppressWarnings("unused")
public class ExtensionEventRegistrar {

    private final Map<Class<? extends AuthLockEvent>, ListenerData> eventListeners = new HashMap<>();

    /**
     * Register the listener
     *
     * @param listener the listener to register
     */
    public void register(final AuthLockListener listener) {
        Preconditions.assertNotNull(listener, "listener");

        Method[] methods = listener.getClass().getMethods();
        for (Method method : methods) {
            Class<?>[] params = method.getParameterTypes();
            if (params.length != 1 || !AuthLockEvent.class.isAssignableFrom(params[0]))
                continue;

            Class<?> parameter = params[0];
            Class<? extends AuthLockEvent> parameterEvent = parameter.asSubclass(AuthLockEvent.class);

            ListenerData data = this.eventListeners.computeIfAbsent(parameterEvent, e -> new ListenerData());
            data.append(listener, parameterEvent);
            if (data.isEmpty())
                this.eventListeners.remove(parameterEvent);
        }
    }

    /**
     * Remove the listener
     *
     * @param listener the listener to remove
     */
    public void remove(final AuthLockListener listener) {
        Set<Class<? extends AuthLockEvent>> toRemove = new HashSet<>();
        for (Map.Entry<Class<? extends AuthLockEvent>, ListenerData> entry : this.eventListeners.entrySet()) {
            ListenerData data = entry.getValue();
            data.remove(listener);

            if (data.isEmpty())
                toRemove.add(entry.getKey());
        }

        toRemove.forEach(this.eventListeners::remove);
    }

    /**
     * Invoke the event
     * @param event the event to invoke
     */
    public void invoke(final AuthLockEvent event) {
        Preconditions.assertNotNull(event, "event");

        Class<? extends AuthLockEvent> eventType = event.getClass();
        ListenerData data = this.eventListeners.get(eventType);

        data.invokeAll(event);
    }
}