/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.authority.exception;

import es.karmadev.authlock.api.authority.AuthorityEntity;
import es.karmadev.authlock.api.authority.AuthorityObject;

/**
 * This exception is thrown when
 * an operation is tried to be executed
 * by an authorization entity which does
 * not have power under an object
 */
public class AuthorizationException extends RuntimeException {

    /**
     * Initialize the exception
     *
     * @param entity the authority entity
     * @param object the authority object
     */
    public AuthorizationException(final AuthorityEntity entity, final AuthorityObject object) {
        super("Failed to perform operation under " + object.getObjectName() + " because entity " + entity.getAuthorityName() + " does not have power under it");
    }
}
