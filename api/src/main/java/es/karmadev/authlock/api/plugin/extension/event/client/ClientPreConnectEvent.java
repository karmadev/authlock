/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.plugin.extension.event.client;

import es.karmadev.authlock.api.plugin.extension.event.AuthLockEvent;
import es.karmadev.authlock.api.plugin.extension.event.Cancellable;
import es.karmadev.authlock.api.util.Preconditions;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.net.SocketAddress;
import java.util.UUID;

/**
 * This event is fired when a client
 * connects to the server, but before
 * all his data is initialized
 */
public class ClientPreConnectEvent extends AuthLockEvent implements Cancellable {

    private final String username;
    private final UUID uniqueId;
    private final SocketAddress address;

    private boolean cancelled;
    private String cancelReason = "&cUser auth ticket has been cancelled";

    /**
     * Create the event
     *
     * @param username the username
     * @param uniqueId the unique id
     */
    public ClientPreConnectEvent(final String username, final UUID uniqueId, final SocketAddress address) {
        Preconditions.assertNotNull(username, "username");
        Preconditions.assertNotNull(uniqueId, "uniqueId");

        this.username = username;
        this.uniqueId = uniqueId;
        this.address = address;
    }

    /**
     * Get the username of the
     * connecting client
     *
     * @return the client username
     */
    @NotNull
    public String getUsername() {
        return this.username;
    }

    /**
     * Get the unique id of the
     * connecting client
     *
     * @return the client unique id
     */
    @NotNull
    public UUID getUniqueId() {
        return this.uniqueId;
    }

    /**
     * Get the address of the
     * connecting client
     *
     * @return the client address
     */
    @Nullable
    public SocketAddress getAddress() {
        return this.address;
    }

    /**
     * Get the reason of the event
     * cancellation. This is also used
     * as the kick message
     *
     * @return the cancel reason
     */
    @NotNull
    public String getCancelReason() {
        return this.cancelReason;
    }

    /**
     * Set the reason of the event
     * cancellation
     *
     * @param reason the cancel reason.
     */
    public void setCancelReason(final String reason) {
        Preconditions.assertNotNull(reason, "reason");
        Preconditions.assertNotEmpty(reason, "reason");

        this.cancelReason = reason;
    }

    /**
     * Get if the event is
     * cancelled
     *
     * @return if the event has
     * been cancelled
     */
    @Override
    public boolean isCancelled() {
        return this.cancelled;
    }

    /**
     * Mark the event has cancelled
     *
     * @param cancelled the cancelled status
     */
    @Override
    public void setCancelled(final boolean cancelled) {
        this.cancelled = cancelled;
    }
}
