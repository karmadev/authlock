/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.auth;

import es.karmadev.authlock.api.auth.process.AuthProcess;
import es.karmadev.authlock.api.auth.process.AuthStep;
import es.karmadev.authlock.api.player.client.OnlineClient;
import es.karmadev.authlock.api.plugin.Manager;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Collections;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.CompletableFuture;

/**
 * Represents the user auth process
 * manager
 */
public class AuthProcessManager implements Manager {

    protected final Queue<AuthStep> steps = new PriorityQueue<>();

    /**
     * Register an auth step
     *
     * @param step the step to register
     */
    public void registerStep(final AuthStep step) {
        this.steps.add(step);
    }

    /**
     * Remove an auth step
     *
     * @param step the step to remove
     */
    public boolean removeStep(final AuthStep step) {
        return this.steps.remove(step);
    }

    /**
     * Get all the auth steps
     *
     * @return the registered steps
     */
    public Collection<AuthStep> getSteps() {
        return Collections.unmodifiableCollection(this.steps);
    }

    /**
     * Create a new auth process
     *
     * @param client the client to create the process
     *               for
     * @return the auth process
     */
    public AuthProcess createProcess(final OnlineClient client) {
        if (this.steps.isEmpty())
            return AuthProcess.completedProcess(client);

        AuthStep[] stepsArray = this.steps.toArray(new AuthStep[0]);
        return new AuthProcess(client, stepsArray);
    }

    /**
     * Prepares the object manager
     * cache
     */
    @Override
    public @NotNull CompletableFuture<Void> prepareCache() {
        return CompletableFuture.completedFuture(null);
    }
}