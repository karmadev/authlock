/*
    This file is part of AuthLock, licensed under the MIT License.

    Copyright (c) karma (KarmaDev) <karmadev.es@gmail.com>
    Copyright (c) contributors

    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

    The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
*/

package es.karmadev.authlock.api.player.session;

import es.karmadev.authlock.api.sequel.entity.SQLEntity;
import es.karmadev.authlock.api.util.data.UnsignedBigInteger;
import org.jetbrains.annotations.NotNull;

import java.time.Instant;
import java.util.concurrent.CompletableFuture;

/**
 * Represents a player session
 */
public interface Session extends SQLEntity {

    /**
     * Get the client of the session
     *
     * @return the session client
     */
    @NotNull
    UnsignedBigInteger getClientId();

    /**
     * Get if the session is
     * password logged
     *
     * @return if password logged
     */
    boolean isPasswordLogged();

    /**
     * Get if the session is
     * pin logged
     *
     * @return if pin logged
     */
    boolean isPinLogged();

    /**
     * Get if the session is
     * totp logged
     *
     * @return if totp logged
     */
    boolean isTotpLogged();

    /**
     * Get the session validity.
     * The session validity just represents
     * the last time {@link #setPasswordLogged(boolean)} was
     * successful for a status value of <code>true</code>
     *
     * @return the session validity
     */
    @NotNull
    Instant getValidity();

    /**
     * Set the account password
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @NotNull
    CompletableFuture<Boolean> setPasswordLogged(final boolean status);

    /**
     * Set the account pin
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @NotNull
    CompletableFuture<Boolean> setPinLogged(final boolean status);

    /**
     * Set the account totp
     * login status
     *
     * @param status the new login status
     * @return if the status was set
     */
    @NotNull
    CompletableFuture<Boolean> setTotpLogged(final boolean status);
}
