[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=karmadeb_authlock)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)
[![pipeline status](https://gitlab.com/karmadev/authlock/badges/master/pipeline.svg)](https://gitlab.com/karmadev/authlock/-/commits/master)

# AuthLock 

Welcome to AuthLock, the replacement for LockLogin.
AuthLock has been built with scalability and optimizations
in mind.

## Development status:

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=karmadeb_authlock&metric=coverage)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=karmadeb_authlock&metric=duplicated_lines_density)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=karmadeb_authlock&metric=code_smells)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=karmadeb_authlock&metric=bugs)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)

### We are using SonarQube
[![SonarCloud](https://sonarcloud.io/images/project_badges/sonarcloud-black.svg)](https://sonarcloud.io/summary/new_code?id=karmadeb_authlock)